const required = (value) => value.trim().length >= 1;
const minLen = (len) => (value) => value.trim().length >= len;
const maxLen = (len) => (value) => value.trim().length <= len;
const emailValidator = (value) => {
    //eslint-disable-next-line
    if (/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value)) {
        return (true)
    } else {
        return (false)

    }
}

const usernameValidator = (value) => {

    if (/^[a-zA-Z0-9]([._-](?![._-])|[a-zA-Z0-9]){3,18}[a-zA-Z0-9]$/.test(value)) {
        return (true)
    } else {
        return (false)

    }
}
const phoneValidator = (value) => {
    if (value.length === 10) {
        if (value[0] !== '0' || value[1] !== '8') {
            return false
        }

        for (let i = 2; i < value.length; i++) {
            if (value[i] < '0' || value[i] > '9') {
                return false;
            }
        }

        return true
    } else if (value.length === 13) {
        if (value[0] !== '+' || value[1] !== '3' || value[2] !== '5' || value[3] !== '9') {
            return false
        }

        for (let i = 4; i < value.length; i++) {
            if (value[i] < '0' || value[i] > '9') {
                return false;
            }
        }

        return true
    } else {
        return false

    }
}
const samePassword = () => (pass, ref) => pass === ref;


const validate = (value, validators) =>
    validators.every((validator) => validator(value));

const validatePass = (value, value2, validators) =>
    validators.every((validator) => validator(value, value2));

const hasWhiteSpace = (s) => {
    let flag = true;
    for (let i = 0; i < s.length; i++) {
        if (s[i] === " ") {
            flag = false;
        }
    }

    return flag;
}

const numberValidator = (value) => {
    if (typeof value != "string") return false // we only process strings!  
    return !isNaN(value) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
        !isNaN(parseFloat(value)) && value > 0 // ...and ensure strings of whitespace fail
}

const numberIntValidator = (value) => {
    if (typeof value != "string") return false // we only process strings!  
    return !isNaN(value) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
        !isNaN(parseFloat(value)) && value > 0 && (+value === parseInt(value, 10)) // ...and ensure strings of whitespace fail
}

export {
    required,
    maxLen,
    minLen,
    emailValidator,
    phoneValidator,
    samePassword,
    validate,
    validatePass,
    hasWhiteSpace,
    numberValidator,
    numberIntValidator,
    usernameValidator
}