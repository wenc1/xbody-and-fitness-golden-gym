import { useEffect, useState } from "react";
import XbodyLocation from "../Components/Common/Location/XbodyLocation";
import functions from "../functions/functions";
import Trainers from './../Components/Common/Trainers/Trainers';
import SubTitle from './../Components/Common/SubTitle/SubTitle';
import Footer from "../Components/Footer/Footer";
import CustomSpinner from "../Components/Common/CustomSpinner/CustomSpinner";
import PricesGym from "../Components/GoldenGym/Prices/PricesGym";
import './SingleXbodyClubContainer.css'
import './XbodyContainer.css';
import Logo from './../Components/Header/Logo.png'
import { workTimeGoldenGym } from "../Constants/constants";
import WorkTime from "../Components/Common/WorkTime/WorkTime";
import PersonalTrainersCards from "../Components/GoldenGym/PersonalTrainesCards/PersonalTrainersCards";

const GoldenGymContainer = () => {
    const [staff, setStaff] = useState([])
    const [loader, setLoader] = useState(true)
    const [typePrices, setTypePrices] = useState("Ученици")

    useEffect(() => {
        setLoader(true)
        functions.getGymstaff(setStaff)
        setLoader(false)
    }, [])


    return (
        <div className="mainSingleXbodyContainer" style={{"position":"absolute"}}>
            <div className="xbodyAllTitleContainer">
                <div style={{ "display": "grid" }}>
                    <img src={Logo} style={{ "width": "350px", "margin": "auto" }} alt=""></img>

                </div>
                <h1 className="xbodyTitle" style={{ "fontSize": "60px" }}>Golden Gym</h1>
                <hr className="mainTitleHr"></hr>
            </div>
            {/* <div> */}
            <p className="xbodyDescription">
                Golden gym е спортна зала в центъра на Благоевград. На площта му от 800кв.м. са разположени специализирани,
                <br></br>
                професионални фитнес уреди, качествено зареден бар с фитнес добавки, масажно студио, солариум, X Body студио и Cryo Sauna.
                <br></br>
               Екипът е изграден от професионалисти, готови да помогнат на всеки клиент да постигне целите си, както и  да бъде в приятна атмосфера.
                <br></br>

                Импулсите се генерират от машина и чрез електроди, положени върху повърхността на кожата, като предизвикват съкращение на мускулите.

            </p>
            {staff.length > 0 && !loader ?
                <div>
                    <SubTitle text={"Персонал"}></SubTitle>
                    <Trainers trainers={staff} type="gym"></Trainers>
                    <SubTitle text={"Персонални услуги"}></SubTitle>
                    <PersonalTrainersCards trainers={staff}></PersonalTrainersCards>
                    <SubTitle text={"Ценоразпис"}></SubTitle>
                    <PricesGym typePrices={typePrices} setTypePrices={setTypePrices}></PricesGym>
                    <SubTitle text={"Работно Време"}></SubTitle>
                    <WorkTime times={workTimeGoldenGym}></WorkTime>
                    <SubTitle text={"Локация"}></SubTitle>
                    <XbodyLocation url={"https://maps.google.com/maps?q=golden%20gym%20blagoevgrad&t=&z=13&ie=UTF8&iwloc=&output=embed"}></XbodyLocation>
                </div> :
                <div>
                    <CustomSpinner></CustomSpinner>
                </div>
                }
            {/* </div> */}
            <Footer></Footer>
        </div>
    )
}

export default GoldenGymContainer;