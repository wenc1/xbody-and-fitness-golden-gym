import { useEffect, useState } from "react";
import ScheduleXbody from "../Components/Common/Schedule/ScheduleXbody";
import functions from "../functions/functions";
import { trainersHours, url } from '../Constants/constants'
import CustomSpinner from "../Components/Common/CustomSpinner/CustomSpinner";
import Footer from "../Components/Footer/Footer";
import SubTitle from "../Components/Common/SubTitle/SubTitle";
import PriceXbody from "../Components/Common/Price/PriceXbody";
import OrderTraining from "../Components/GoldenGym/OrderTraining/OrderTraining";

const SingleGymTrainerContainer = (props) => {
    const { id } = props.match.params;
    const [trainer, setTrainer] = useState(null)
    const [schedule, setSchedule] = useState([])

    useEffect(() => {
        functions.getGymstaffSingle(id, setTrainer)
        functions.getSingleTrainerSchedule(id, setSchedule)
    }, [id])

    return (
        <div className="mainSingleXbodyContainer" style={{ "position": "absolute" }}>
            {schedule.length > 0 && trainer ? (
                <div>
                    <div className="singleXbodyTitleContainer" >
                        <h1 className="singlexbodyTitle">Тренирай с <br></br> {trainer[0].name}</h1>
                        <hr className="singleXbodyHr"></hr>
                    </div>
                    <div style={{
                        "width": "100%",
                        "display":"inline-grid",
                        "justifyContent": "center",
                        "marginTop": "20px"
                    }}>
                        <img src={`${url}/public/${trainer[0].photo}`} style={{"width":"250px","margin":"auto","borderRadius":"50%"}}></img>
                    </div>
                    <SubTitle text={"Запази час за персонална тренировка"}></SubTitle>
                    <ScheduleXbody schedulerTest={schedule} id={id} setSchedule={setSchedule} times={trainersHours} type="gym"></ScheduleXbody>
                    <SubTitle text={"Поръчай пероснална тренировъчна програма и хранителен план"}></SubTitle>
                    <PriceXbody name="ПерсоналниЦени"></PriceXbody>
                    <OrderTraining trainer={trainer[0]}></OrderTraining>
            :
                </div>
            ) :
                <CustomSpinner></CustomSpinner>}
            <Footer></Footer>
        </div>


    )
}

export default SingleGymTrainerContainer;