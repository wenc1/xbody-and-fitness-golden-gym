import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import './HomePageContainer.css'

const HomePageContainer = () => {
    useEffect(() => {
        window.scrollTo(0, 0)
    },[])

    return (
        <div className="container-homepage">
            <div className="split left" >
                <h1 className="homeTitle">XBody Wellness</h1>
                <Link to="/xbody" className="homebutton">Продължи</Link>
            </div>
            <div className="split right">
                <div className="right">
                    <h1 className="homeTitle">Golden Gym</h1>
                    <Link to="/gym" className="homebutton">Продължи</Link>
                </div>

            </div>
        </div>
    )
}

export default HomePageContainer;