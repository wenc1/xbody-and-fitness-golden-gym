import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import AllXbodyClubs from './../Components/Xbody/AllXbodyClubs';
import functions from './../functions/functions.js';
import './XbodyContainer.css';
import Footer from './../Components/Footer/Footer';
import CustomSpinner from '../Components/Common/CustomSpinner/CustomSpinner';
import SubTitle from '../Components/Common/SubTitle/SubTitle';
import xbodylogo from './../Components/Xbody/Images/xbodylogo.png';

const XbodyContainer = () => {
    const [clubs, setClubs] = useState([]);
    const [loader, setLoader] = useState(true);

    useEffect(() => {
        functions.getAllXbodyClubs(setClubs, setLoader);
    }, [])

    return (
        <div className="mainXbodyContainer" style={{"position":"absolute"}}>
            <div className="xbodyAllTitleContainer">
                <h1 className="xbodyTitle">
                    <img className="xbodyTitleImg"
                        src={xbodylogo} alt="">
                    </img>Xbody
                </h1>
                <hr className="mainTitleHr"></hr>
            </div>
            {clubs.length > 0 && !loader ? <>
                <SubTitle text={"Какво е XBODY?"}></SubTitle>
                <div style={{ "display": "grid" }}>
                    <img className="xbodyImg" src="https://i.pinimg.com/originals/bd/45/88/bd4588ce846dacb366032d2a3c2b0288.jpg" alt=""></img>
                </div>
                <p className="xbodyDescription">
                    Xbody е иновативен метод за тренировка и рехабилитация чрез електро-мускулна стимулация на тялото (EMC).
                <br></br>
                EMC представлява предизвикване на мускулна контракция чрез електрически импулси.
                <br></br>
                Принципът е приложим за тренировка, рехабилитация и разкрасяване.
                <br></br>
                Импулсите се генерират от машина и чрез електроди, положени върху повърхността на кожата, като предизвикват съкращение на мускулите.
              <br></br>
              Резултатът е подобен на действителното движение на мускулите при полагане на усилие.
            </p>
                <SubTitle text={"Нашите зали"}></SubTitle>
                {clubs.length > 0
                    ? <AllXbodyClubs clubs={clubs} ></AllXbodyClubs>
                    : null}
                    
                <SubTitle text={"Франчайз"}></SubTitle>

                <p className="xbodyDescription" >
                    <b>Искате да започнете свой бизнес?</b>
                    <br></br>
                    <Link to="#" style={{ "color": "#77601d" }}>
                        <b>Влезте тук, за да Ви помогнем!</b></Link>
                </p>
            </> :
                <CustomSpinner></CustomSpinner>
            }


            <Footer></Footer>
        </div>
    )
}

export default XbodyContainer;

