import React, { useEffect, useState } from 'react';
import './SingleXbodyClubContainer.css'
import Trainers from './../Components/Common/Trainers/Trainers';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import ScheduleXbody from '../Components/Common/Schedule/ScheduleXbody';
// import CarouselXbody from '../Components/Common/Carousel/CarouselXbody';
import XbodyLocation from '../Components/Common/Location/XbodyLocation';
import functions from './../functions/functions.js'
import { hoursBlagoevgrad, otherHours, returnXbodyWokrTimes, url } from '../Constants/constants'
import PriceXbody from '../Components/Common/Price/PriceXbody';
import Footer from './../Components/Footer/Footer'
import SubTitle from './../Components/Common/SubTitle/SubTitle'
import CustomSpinner from '../Components/Common/CustomSpinner/CustomSpinner';
import WorkTime from '../Components/Common/WorkTime/WorkTime';

const SingleXbodyClubContainer = (props) => {

    const { id } = props.match.params;
    const [trainers, setTrainers] = useState([]);
    const [clubInfo, setClubInfo] = useState([]);
    const [schedule, setSchedule] = useState([]);
    const [loader, setLoader] = useState(true);

    useEffect(() => {
        setLoader(true)
        functions.getSingleXbodyClubData(id, setTrainers, setSchedule, setClubInfo, setLoader)
        setLoader(false)

    }, [id])

    return (
        <div className="mainSingleXbodyContainer" style={{"position":"absolute"}}>
            {schedule.length > 0 && clubInfo.length > 0 && trainers.length > 0 && !loader ? (
                <div>
                    <div className="singleXbodyTitleContainer" >
                        <h1 className="singlexbodyTitle"><img src={`${url}/public/xbodylogo.png`} className="singleXbodyPhoto" alt=""></img>Xbody {clubInfo[0].name}</h1>
                        <hr className="singleXbodyHr"></hr>
                    </div>
                    <SubTitle text={"Треньори"}></SubTitle>
                    <Trainers trainers={trainers} type="xbody" ></Trainers>
                    <SubTitle text={"График"}></SubTitle>
                    {clubInfo[0].name === "Благоевград"
                        ? <ScheduleXbody schedulerTest={schedule} id={id} setClubInfo={setClubInfo} setSchedule={setSchedule} setTrainers={setTrainers} times={hoursBlagoevgrad} type="xbody"></ScheduleXbody>
                        : <ScheduleXbody schedulerTest={schedule} id={id} setClubInfo={setClubInfo} setSchedule={setSchedule} setTrainers={setTrainers} times={otherHours} type="xbody"></ScheduleXbody>
                    }
                    <SubTitle text={"Цени"}></SubTitle>
                    <PriceXbody name={clubInfo[0].name}></PriceXbody>
                    <SubTitle text={"Работно време"}></SubTitle>
                    <WorkTime times={returnXbodyWokrTimes(clubInfo[0].name)}></WorkTime>
                    <SubTitle text={"Локация"}></SubTitle>
                    <XbodyLocation url={clubInfo[0].location}></XbodyLocation>
                </div>
            ) :
                <CustomSpinner></CustomSpinner>}
            <Footer></Footer>
        </div>
    )
}

export default SingleXbodyClubContainer;

