// import './App.css';
import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import HomePageContainer from './Containers/HomePageContainer';
import XbodyContainer from './Containers/XbodyContainer';
import SingleXbodyClubContainer from './Containers/SingleXbodyClubContainer.js';
import Header from './Components/Header/Header'
import { Switch, BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import LogIn from './Components/AuthCopmponents/LogIn';
import AuthContext, { extractUser, getToken } from './Providers/AuthContext';
import Register from './Components/AuthCopmponents/Register';
import ResetPassword from './Components/AuthCopmponents/ResetPassword';
import { ToastProvider } from 'react-toast-notifications';
import GoldenGymContainer from './Containers/GoldenGymContainer';
import SingleGymTrainerContainer from './Containers/SingleGymTrainerContainer';
import ScrollToTop from './Components/ScrollTopTop/ScrollToTop';

function App(props) {
  const [authValue, setAuthValue] = useState({
    isLoggedIn: !!extractUser(getToken()),
    user: extractUser(getToken())
  });

  return (
    <Router >
      <ScrollToTop />
      <AuthContext.Provider value={{ ...authValue, setLoginState: setAuthValue }}>
        <ToastProvider>
          <Header></Header>
          {!authValue.isLoggedIn &&
            <Switch>
              <Route path='/login' exact component={LogIn} />
              <Route path='/register' exact component={Register} />
              <Route path='/reset' exact component={ResetPassword} />
            </Switch>
          }
          <Switch>
            <Redirect path='/' exact to='/home' />
            <Route path='/home' exact component={HomePageContainer} />
            <Route path='/xbody' exact component={XbodyContainer} />
            <Route path='/xbody/:id' exact component={SingleXbodyClubContainer} />
            <Route path='/gym' exact component={GoldenGymContainer} />
            <Route path='/gym/trainer/:id' exact component={SingleGymTrainerContainer} />
          </Switch>
        </ToastProvider>
      </AuthContext.Provider>
    </Router>
  );
}

export default App;
