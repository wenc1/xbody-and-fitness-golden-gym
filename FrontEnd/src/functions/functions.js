import {
    url,
    TOKEN_LIFETIME
} from '../Constants/constants';

const getAllXbodyClubs = (setClubs, setLoader) => {
    setLoader(true)
    fetch(`${url}/user/clubs`)
        .then((response) => response.json())
        .then((data) => {
            if (data.result) {
                setClubs([...data.result])
            }
        })
        .finally(setLoader(false))
}

const getSingleXbodyClubData = (id, setTrainers, setSchedule, setClubInfo, setLoader) => {
    fetch(`${url}/user/club/${id}/info`)
        .then(response => response.json())
        .then(data => {
            if (data.result) {
                setTrainers([...data.result.trainers])
                setSchedule([...data.result.hours])
                setClubInfo([...data.result.club])
            }
        })
}


const reserveXbodyHour = (id, column, token, t, user, addToast, clubid, setTrainers, setSchedule, setClubInfo) => {
    console.log(t)
    fetch(`${url}/user/schedule/${id}/${column}`, {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/json',
        },
    })
        .then((response) => response.json())
        .then((data) => {
            if (data.result) {
                getSingleXbodyClubData(clubid, setTrainers, setSchedule, setClubInfo)
                addToast("Успешно запазихте час.", {
                    appearance: 'success',
                    autoDismiss: true,
                })
            }else if (data.error === "client error"){
                addToast("Не можете да запазите повече от 1 час девно.", {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        })
}

const deleteXbodyHour = (id, column, token, addToast, clubid, setTrainers, setSchedule, setClubInfo) => {
    fetch(`${url}/user/schedule/${id}/${column}`, {
        method: 'DELETE',
        headers: {
            Authorization: `Bearer ${token}`,
        },
    })
        .then((response) => response.json())
        .then((data) => {
            if (data.result) {
                getSingleXbodyClubData(clubid, setTrainers, setSchedule, setClubInfo)
                addToast("Вашият час е отменен успешно.", {
                    appearance: 'info',
                    autoDismiss: true,
                })
            }
        })
}

const logIn = (username, password, setLoginState, extractUser, addToast, props) => {
    let data = username;

    if (
        // eslint-disable-next-line 
        !(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(username))
        && username.slice(0, 1) === "0"
    ) {
        data = "+359" + username.slice(1)
    }

    fetch(`${url}/auth/signin`, {
        method: 'POST',
        body: JSON.stringify({
            data: data,
            password: password,
        }),
        headers: {
            'Content-Type': 'application/json',
        },
    })
        .then((res) => res.json())
        .then((data) => {
            if (data.token) {
                // eslint-disable-next-line
                localStorage.setItem('token', data.token);
                setLoginState({
                    isLoggedIn: !!extractUser(data.token),
                    user: extractUser(data.token),
                });
                addToast("Добре дошли.", {
                    appearance: 'success',
                    autoDismiss: true,
                })

                props.history.push('/home')
                setTimeout(() => {
                    logOut(setLoginState)
                }, TOKEN_LIFETIME * 1000)
            } else {
                addToast("Невалиди потребителски данни.", {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        })
}

const logOut = (setLoginState) => {
    setLoginState(false);
    localStorage.removeItem('token');
    //   props.history.push('/home');
};

const register = (username, email, firstName, lastName, phoneNumber, password, props, addToast) => {
    let phone = phoneNumber;

    if (phoneNumber.slice(0, 1) === "0") {
        phone = "+359" + phoneNumber.slice(1)
    }

    fetch(`${url}/auth/signup`, {
        method: 'POST',
        body: JSON.stringify({
            username: username,
            email: email,
            firstName: firstName,
            lastName: lastName,
            phoneNumber: phone,
            password: password,
            rolesId: 3
        }),
        headers: {
            'Content-Type': 'application/json',
        },
    })
        .then((res) => res.json())
        .then((data) => {
            if (data.result) {
                addToast("Вашата регистрация е успешна.", {
                    appearance: 'success',
                    autoDismiss: true,
                })
                props.history.push('/login');
            } else {
                addToast("Вашата регистрация е неуспешна.", {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        })
};


const sentEmail = (mail, addToast, emailjs) => {
    let max = 10000;
    let min = 1000;
    const code = Math.floor(Math.random() * (max - min)) + min;
    const userID = "user_nANhnJK4xSlh7xWpguHfV";

//eslint-disable-next-line
    if (/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(mail)) {
        emailjs.send("service_agfp7rc", "template_mr3d1ex", {
            code: code,
            to_email: mail,
        }, userID);
        fetch(`${url}/auth/key`, {
            method: 'POST',
            body: JSON.stringify({
                email: mail,
                key: code,
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then((res) => res.json())
            .then((data) => {
                if (data.result) {
                    addToast("Вашият код е изпратен успешно.", {
                        appearance: 'success',
                        autoDismiss: true,
                    })
                    setTimeout(function () {
                        fetch(`${url}/auth/key`, {
                            method: 'DELETE',
                            body: JSON.stringify({
                                email: mail,
                            }),
                            headers: {
                                'Content-Type': 'application/json',
                            },
                        })
                    }, 300000);

                } else {
                    addToast("Въведеният ймейл е невалиден.", {
                        appearance: 'error',
                        autoDismiss: true,
                    })
                }
            })

    } else {
        addToast("Вашият иимейл адрес е невалиден.", {
            appearance: 'error',
            autoDismiss: true,
        })
    }

}


const sentTrainerEmail = (addToast, emailjs, user, data, trainer) => {
    const userID = "user_nANhnJK4xSlh7xWpguHfV";

    // eslint-disable-next-line
    if (/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(trainer.email)) {
        emailjs.send("service_agfp7rc", "template_8y1deui", {
            to_name: trainer.name,
            to_email: trainer.email,
            phone: user.phone,
            from_name: user.firstname + " " + user.lastname,
            f1: data.f1,
            f2: data.f2,
            f3: data.f3,
            f4: data.f4,
            f5: data.f5,
            f6: data.f6,
            f7: data.f7,
            f8: data.f8,
            f9: data.f9,
            f10: data.f10,
            f11: data.f11,
            f12: data.f12,
            q1: data.q1,
            q2: data.q2,
            q3: data.q3,
            q4: data.q4,
            q5: data.q5,
            q6: data.q6,
            q7: data.q7,

        }, userID)
            .then((result) => {
                if (result.text === "OK") {
                    addToast("Успешна поръчка.", {
                        appearance: 'success',
                        autoDismiss: true,
                    })
                }
            }, (error) => {
                console.log(error.text);
            });

    } else {
        addToast("Вашият иимейл адрес е невалиден.", {
            appearance: 'error',
            autoDismiss: true,
        })
    }

}


const changePassword = (props, email, password, key, addToast) => {
    fetch(`${url}/auth/password`, {
        method: 'PUT',
        body: JSON.stringify({
            email: email,
            key: key,
            password: password
        }),
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then((response) => response.json())
        .then((data) => {
            if (data.result) {
                addToast("Вашата парола е сменена успешно.", {
                    appearance: 'success',
                    autoDismiss: true,
                })
                props.history.push('/login');

            } else {
                addToast("Въведените от вас данни са невалидни.", {
                    appearance: 'error',
                    autoDismiss: true,
                })
            }
        })
}

const getSingleTrainer = (id, setTrainerInfo) => {
    fetch(`${url}/user/trainer/${id}`)
        .then(response => response.json())
        .then(data => {
            if (data.result) {
                setTrainerInfo([...data.result])
            }
        })
}

const getGymstaff = (setStaff) => {
    fetch(`${url}/user/gym/staff`)
        .then(response => response.json())
        .then(data => {
            if (data.result) {
                setStaff([...data.result])
            }
        })
}


const getGymstaffSingle = (id, setSingleStaff) => {
    fetch(`${url}/user/gym/staff/${id}`)
        .then(response => response.json())
        .then(data => {

            if (data.result) {
                setSingleStaff([...data.result])
            }
        })
}

const getSingleTrainerSchedule = (id, setSchedule, setLoader) => {
    fetch(`${url}/user/gym/trainer/schedule/${id}`)
        .then(response => response.json())
        .then(data => {
            if (data.result) {
                setSchedule([...data.result.hours])
            }
        })
}


const reserveGymHour = (id, column, token, addToast, clubid, setSchedule) => {
    fetch(`${url}/user/schedulegym/${id}/${column}`, {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/json',
        },
    })
        .then((response) => response.json())
        .then((data) => {
            if (data.result) {
                getSingleTrainerSchedule(clubid, setSchedule)
                addToast("Успешно запазихте час.", {
                    appearance: 'success',
                    autoDismiss: true,
                })
            }
        })
}

const deleteGymHour = (id, column, token, addToast, clubid, setSchedule) => {
    fetch(`${url}/user/schedulegym/${id}/${column}`, {
        method: 'DELETE',
        headers: {
            Authorization: `Bearer ${token}`,
        },
    })
        .then((response) => response.json())
        .then((data) => {
            if (data.result) {
                getSingleTrainerSchedule(clubid, setSchedule)
                addToast("Вашият час е отменен успешно.", {
                    appearance: 'info',
                    autoDismiss: true,
                })
            }
        })
}

// eslint-disable-next-line 
export default {
    getAllXbodyClubs,
    getSingleXbodyClubData,
    reserveXbodyHour,
    deleteXbodyHour,
    logIn,
    logOut,
    register,
    sentEmail,
    changePassword,
    getSingleTrainer,
    getGymstaff,
    getGymstaffSingle,
    getSingleTrainerSchedule,
    reserveGymHour,
    deleteGymHour,
    sentTrainerEmail
}