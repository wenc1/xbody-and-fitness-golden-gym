const url = 'http://79.100.29.162:4000';
const hoursBlagoevgrad = ["8 : 00", "8 : 40", "9 : 20", "10 : 00", "10 : 40", "11 : 20", "12 : 00", "12 : 40", "13 : 20", "14 : 00", "14 : 40", "15 : 20", "16 : 00", "16 : 40", "17 : 20", "18 : 00", "18 : 40", "19 : 20"];
const otherHours = ["8 : 00", "8 : 30", "9 : 00", "09 : 30", "10 : 00", "10 : 30", "11 : 00", "11 : 30", "12 : 00", "12 : 30", "13 : 00", "13 : 30", "14 : 00", "14 : 30", "15 : 00", "15: 30", "16 : 00", "16 : 30", "17 : 00", "17 : 30", "18 : 00", "18 : 30", "19 : 00", "19 : 30"];
const trainersHours = ["8 : 00", "9 : 00", "10 : 00", "11 : 00", "12 : 00", "13 : 00", "14 : 00", "15 : 00", "16 : 00", "17 : 00", "18 : 00", "19 : 00", "20 : 00"];


//localhost
//79.100.29.162
//goldengymBulgaria@gmail.com
//123aA123!


// parola1
//xbody.sofia.goldengym@mail.bg
//xbody.plovdiv.goldengym@mail.bg
//xbody.blagoevgrad.goldengym@mail.bg
// l.nikolov.goldengymbg@mail.bg
// v.trenchev.goldengymbg@mail.bg
// k.stoyrimski.goldengymbg@mail.bg


const pricesSofia = [
    {
        label: "Единична процедура",
        price: 48
    },
    {
        label: "Пакет 8 процедури",
        price: 280
    },
]

const pricesBlagoevgrad = [{
    label: "Пръва процедура",
    price: 30
},
{
    label: "Единична процедура",
    price: 40
},
{
    label: "Пакет 4 процедури",
    price: 150
},
{
    label: "Пакет 8 процедури",
    price: 280
},
{
    label: "Пакет 12 процедури",
    price: 390
},
{
    label: "Пакет 16 процедури",
    price: 480
}]

const pricesPlovdiv = [
    {
        label: "Единична процедура",
        price: 40
    },
    {
        label: "Пакет 4 процедури",
        price: 150
    },
    {
        label: "Пакет 8 процедури",
        price: 280
    },
    {
        label: "Пакет 12 процедури",
        price: 390
    },
    {
        label: "Пакет 16 процедури",
        price: 480
    }
]


const pricesTrainers = [
    {
        label: "Тренировъчна програма",
        price: 40
    },
    {
        label: "Хранителен план",
        price: 40
    },
    {
        label: "Тренировъчна програма и Хранителен план",
        price: 70
    },


]

const returnPrices = (name) => {
    if (name === "София") {
        return pricesSofia
    }

    if (name === "Пловдив") {
        return pricesPlovdiv
    }

    if (name === "Благоeвград") {
        return pricesBlagoevgrad
    }

    if (name === "ПерсоналниЦени") {
        return pricesTrainers
    }
}

const pricesGymStudent = [
    {
        label: "Единично посещение",
        price: 3.50
    },
    {
        label: "Неограниячена карта",
        price: 36
    },
    {
        label: "Карта 16 посещения",
        price: 32
    },
    {
        label: "Карта 12 посещения",
        price: 28
    },
    {
        label: "3-месечна карта",
        price: 69
    },
    {
        label: "6-месечна карта",
        price: 127
    },
    {
        label: "9-месечна карта",
        price: 180
    },
    {
        label: "12-месечна карта",
        price: 227
    },
]


const pricesGymBachelor = [
    {
        label: "Единично посещение",
        price: 4
    },
    {
        label: "Неограниячена карта",
        price: 39
    },
    {
        label: "Карта 16 посещения",
        price: 36
    },
    {
        label: "Карта 12 посещения",
        price: 32
    },
    {
        label: "3-месечна карта",
        price: 79
    },
    {
        label: "6-месечна карта",
        price: 144
    },
    {
        label: "9-месечна карта",
        price: 200
    },
    {
        label: "12-месечна карта",
        price: 250
    },
]

const pricesGyAdult = [
    {
        label: "Единично посещение",
        price: 4
    },
    {
        label: "Неограниячена карта",
        price: 42
    },
    {
        label: "Карта 16 посещения",
        price: 38
    },
    {
        label: "Карта 12 посещения",
        price: 34
    },
    {
        label: "3-месечна карта",
        price: 89
    },
    {
        label: "6-месечна карта",
        price: 160
    },
    {
        label: "9-месечна карта",
        price: 220
    },
    {
        label: "12-месечна карта",
        price: 280
    },
]

const returnGymPrices = (name) => {
    if (name === "Ученици") {
        return (
            pricesGymStudent
        )
    }

    if (name === "Студенти") {
        return (
            pricesGymBachelor
        )
    }

    if (name === "Възрастни") {
        return (
            pricesGyAdult
        )
    }
}

const workTimeGoldenGym = [
    {
        label: "Понеделник - Петък",
        hours: "07:00ч. - 23:30ч."
    },
    {
        label: "Стбота - Неделя",
        hours: "07:00ч. - 22:00ч."
    }
]

const workTimeCbodySofia = [
    {
        label: "Понеделник - Неделя",
        hours: "08:00ч. - 20:00ч."
    },
]

const workTimeCbodyPlovdiv = [
    {
        label: "Понеделник - Неделя",
        hours: "08:00ч. - 20:00ч."

    },

]

const workTimeCbodyBlagoevgrad = [
    {
        label: "Понеделник - Неделя",
        hours: "08:00ч. - 20:00ч."
    },

]

const returnXbodyWokrTimes = (n) => {
    if (n === "София") {
        return workTimeCbodySofia
    }

    if (n === "Благоeвград") {
        return workTimeCbodyBlagoevgrad

    }

    if (n === "Пловдив") {
        return workTimeCbodyPlovdiv

    }
}

const baseQuestion = [
    "Тренировъчна програма",
    "Хранителен план",
    "Тренировъчна програма и хранителен план"
]
const questionOne = [
    "Почти никаква",
    "Често ходя пеша",
    "Тренирам 1-2 пъти седмично",
    "Тренирам 3-5 пъти седмично",
    "Тренирам 6-7 пъти седмично"
]

const questionTwo = [
    "Не се наспивам достатъчно",
    "Хапвам късно вечер",
    "Консумирам много сол",
    "Не мога без сладко",
    "Обичам газирани напитки",
    "Пия алкохол",
    "Нито едно от изброените"
]

const questionThree = [
    "1 път",
    "2 пъти",
    "3 пъти",
    "4 пъти",
    "Всеки ден различно"
]


const questionFour = [
    "Говеждо",
    "Риба",
    "Скариди",
    "Пилешко",
    "Свинско",
    "Пуешко"
]

const questionFive = [
    "Авокадо",
    "Картоф",
    "Зелени салати",
    "Ориз",
    "Маслини",
    "Зелен фасул",
    "Овесени ядки",
    "Киноа",
    "Сладък картоф",
    "Червено цвекло",
    "Моркови",
    "Домат",
    "Краставица",
    "Аспержи",
    "Броколи"
]

const questionSix = [
    "Яйца",
    "Гъби",
    "Ядки",
    "Кисело мляко",
    "Сирене",
    "Извара",
    "Котидж сиирене",
    "Оризовки"
]

const questionSeven = [
    "Портокал",
    "Тиква",
    "Горски плодове",
    "Ябълки",
    "Банан",
    "Ананас",
    "Грейпфрут",
    "Пъпеш",
    "Диня",
    "Праскова",
    "Ягоди",
    "Малини"

]

const TOKEN_LIFETIME = 1200 * 60
export {
    url,
    hoursBlagoevgrad,
    otherHours,
    returnPrices,
    returnGymPrices,
    workTimeGoldenGym,
    returnXbodyWokrTimes,
    trainersHours,
    pricesTrainers,
    questionOne,
    questionTwo,
    questionThree,
    questionFour,
    questionFive,
    questionSix,
    questionSeven,
    baseQuestion,
    TOKEN_LIFETIME
}