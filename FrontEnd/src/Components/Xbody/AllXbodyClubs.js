import React from 'react'
import './../../ThemeCSSFiles/css/main.css'
import './../../ThemeCSSFiles/css/media.css'
import './../../ThemeCSSFiles/css/fonts.css'
import {
    url
} from '../../Constants/constants';

const AllXbodyClubs = ({ clubs }) => {
    const clubsSingleView = clubs.map((club) => {
        if (+club.id % 2 !== 0) {
            return (
                <section className="about" id="our_fitness" key={"club_" + club.id} style={{ "paddingBottom": "20px" }}>
                    <div className="about-row">
                        <div className="about-row__item">
                            <div style={{ "display": "flex" }}>
                                <img src={`${url}/public/` + club.main_photo} style={{ "margin": "auto" }} alt="altPicture"></img>
                            </div>
                        </div>
                        <div className="about-row__item">
                            <h2 className="about-title">{club.name}</h2>
                            <hr className="clubsHr"></hr>

                            <div className="mobile-photo-container">
                                <img className="mobile-photo" src={`${url}/public/` + club.main_photo} alt="altPicture"></img>
                            </div>
                            <article className="about_text">
                                <div>
                                    <p>{club.description}</p>
                                    <div className="member__link">
                                        <a href={"/xbody/" + club.id}> <b>Продължи</b></a>

                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </section>
            )
        }
        else {
            return (
                <section className="about" id="our_fitness" key={"club_" + club.id} style={{ "paddingBottom": "20px" }}>
                    <div className="about-row">
                        <div className="about-row_text">
                            <h2 className="about-title">{club.name}</h2>
                            <hr className="clubsHr"></hr>
                            <div className="mobile-photo-container">
                                <img className="mobile-photo" src={`${url}/public/` + club.main_photo} alt="altPicture"></img>
                            </div>
                            <article className="about_text">
                                <div>
                                    <p>{club.description}</p>
                                    <div className="member__link">
                                        <a href={"/xbody/" + club.id}><b>Продължи</b></a>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div className="about-row_photo" style={{"margin":"auto"}}>
                            <div style={{ "display": "flex" }}>
                                <img src={`${url}/public/` + club.main_photo} style={{ "margin": "auto" }} alt="altPicture"></img>

                            </div>
                        </div>
                    </div>
                </section>
            )
        }
    })
    return (
        <div>
            {clubsSingleView}
        </div>
    )
}

export default AllXbodyClubs