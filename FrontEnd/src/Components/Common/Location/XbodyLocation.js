import './XbodyLocation.css'
const XbodyLocation = ({ url }) => {
    return (
        <div className="xbodyLocation">
            <div className="gmap_canvas" style={{ "width": "100%" }}><iframe
                title="map" className="frame" id="gmap_canvas"
                src={url + "&output=embed"}
                frameBorder={0}
                scrolling="no"
                marginHeight={0}
                marginWidth={0}
                style={{ "width": "80%", "marginLeft": "10%", "height": "150px" }} />
            </div>
        </div>

    )
}

export default XbodyLocation;