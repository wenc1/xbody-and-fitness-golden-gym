const SubTitle = ({ text }) => {
    return (
        <div className="subTitleSingleXbodyContainer">
            <h2 className="subTitleSingleXbodyText">{text}</h2>
            <hr className="subTitleSingleXbodyHr"></hr>
        </div>
    )
}

export default SubTitle;