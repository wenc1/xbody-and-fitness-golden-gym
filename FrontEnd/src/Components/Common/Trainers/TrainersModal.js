import React, { useEffect, useState } from 'react';
import './TrainersModal.css'
import functions from '../../../functions/functions'
import { url } from '../../../Constants/constants';
import CustomSpinner from '../CustomSpinner/CustomSpinner'

const TrainersModal = ({ setModalState, modalState, trainerId, setTrainerId, type }) => {
    const [trainerInfo, setTrainerInfo] = useState([])
    const [loader, setLoader] = useState(false)
    useEffect(() => {
        setLoader(true)
        // if (trainerId) {
        if (type === "xbody") {
            functions.getSingleTrainer(trainerId, setTrainerInfo)
        }
        if (type === "gym") {
            functions.getGymstaffSingle(trainerId, setTrainerInfo)
        }
        // }
        setLoader(false)

    }, [type, trainerId])

    return (
        <div className={`modalBackground modalShowing-${modalState}`}>
            {trainerInfo.length > 0 && !loader ? <div className="modalInner">
                <div className="modalImage">
                    {trainerInfo[0].photo ? <img className="profile-picture" src={`${url}/public/` + trainerInfo[0].modal_photo} alt=""></img> : <img
                        src="https://images.unsplash.com/photo-1586931775007-ad3c3913f216?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1300&q=80"
                        alt="modal pic"
                    />}

                </div>
                <div className="modalText">
                    <h2>{trainerInfo[0].name}</h2>
                    <p>
                        {trainerInfo[0].description}
                    </p>
                    <button
                        className='btn'
                        onClick={() => { setModalState(false); }}
                        style={{ "backgroundColor": "#b9962da4" }}
                    >
                        Изход
        </button>

                </div>
            </div> :
                <CustomSpinner></CustomSpinner>
            }
        </div>
    )

}

export default TrainersModal;