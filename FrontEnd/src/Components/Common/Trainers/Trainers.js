import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebookF } from '@fortawesome/free-brands-svg-icons'
import { faInstagram } from "@fortawesome/free-brands-svg-icons"
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import TrainersModal from './TrainersModal';
import { faInfo } from '@fortawesome/free-solid-svg-icons';
import { url } from '../../../Constants/constants.js';
import './Trainers.css'

const Trainers = ({ trainers, type }) => {
    const [modalState, setModalState] = useState([])
    const [trainerId, setTrainerId] = useState(null)

    const trainersView = trainers.map((trainer) => {
        return (
            <Col lg={4} xs={9} sm={6} md={4} key={"trainer" + trainer.id + trainer.name}>
                <div className="singleTrainer">
                    <figure className="team-photo">
                        {trainer.photo ?
                            <img className="profile-picture" src={`${url}/public/` + trainer.photo} alt=""></img>
                            : <img className="profile-picture" src="https://www.shareicon.net/data/2016/07/05/791224_man_512x512.png" alt=""></img>}
                    </figure>
                    <h3 className="trainer-name" style={{ "fontFamily": "Oswald-Medium" }} >{trainer.name}</h3>
                    <h5 className="profesion">{trainer.position}</h5>
                    <div className="social-menu">
                        <div className="iconTrainer">
                            <FontAwesomeIcon style={{ "cursor": "pointer" }} icon={faFacebookF} className="social-icon"></FontAwesomeIcon>
                        </div>
                        <div className="iconTrainer">
                            <FontAwesomeIcon style={{ "cursor": "pointer" }} icon={faInstagram} className="social-icon"></FontAwesomeIcon>
                        </div>
                        <div className="iconTrainer">
                            <FontAwesomeIcon style={{ "cursor": "pointer" }} icon={faInfo} className="social-icon" onClick={(e) => { e.preventDefault(); setModalState(true); setTrainerId(trainer.id) }}></FontAwesomeIcon>
                        </div>
                    </div>
                </div>
            </Col>
        )
    })


    return (
        <div className="pageElement">

            <Container>
                <Row style={{ "justifyContent": "center" }}>
                    {trainersView}
                </Row>
            </Container>
            {modalState && trainerId ?
                <TrainersModal setModalState={setModalState} modalState={modalState} trainerId={trainerId} setTrainerId={setTrainerId} type={type}></TrainersModal>

                : null}
        </div>
    )
}

export default Trainers;