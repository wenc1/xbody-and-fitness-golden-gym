import { Spinner } from "react-bootstrap";

const CustomSpinner = () => {
    return (
        <div style={{ "textAlign": "center","width":"100%" }}>
            <Spinner animation="border" style={{ "width": "150px", "height": "150px", "color": "#b9962da4", "marginTop": "80px", "marginBottom": "80px" }}></Spinner>
        </div>
    )

}

export default CustomSpinner;