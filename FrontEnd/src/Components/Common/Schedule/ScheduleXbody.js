import React, { useContext, useState } from 'react';
import Table from 'react-bootstrap/Table';
import { Link } from 'react-router-dom';
import AuthContext from '../../../Providers/AuthContext';
import Pagination from './Pagination';
import { useToasts } from 'react-toast-notifications';
import functions from '../../../functions/functions';
import './ScheduleXbody.css';

const ScheduleXbody = ({ schedulerTest, times, id, setClubInfo, setSchedule, setTrainers, type }) => {
    const { user } = useContext(AuthContext);
    const token = localStorage.getItem('token');
    const { addToast } = useToasts()

    const [postsPerPage] = useState(7);
    const [currentPage, setCurrentPage] = useState(1);

    const indexOfLastPost = currentPage * postsPerPage;
    const indexOfFirstPost = indexOfLastPost - postsPerPage;
    const currentPosts = schedulerTest.slice(indexOfFirstPost, indexOfLastPost);

    // Change page
    const paginate = pageNumber => setCurrentPage(pageNumber);

    return (
        <div className="pageElement">

            <div>
                <Table responsive striped bordered hover variant="dark" className="Table" size="sm">
                    <thead>
                        <tr>
                            <th className="tableTh" >Часове</th>
                            {currentPosts.length > 0 ?
                                currentPosts.map((el, index) => {
                                    return (
                                        <th key={"date_" + index + type} className="tableTh" >{el.date.slice(0, 5)}</th>
                                    )
                                })
                                : null}
                        </tr>
                    </thead>
                    <tbody>

                        {times.map((t, index) => {
                            return (
                                <tr key={"ttr" + index + type}>
                                    <td className="tableTd" >{t}</td>
                                    {currentPosts.map((el, i) => {
                                        return (
                                            user ? <td key={"value" + i} className="tableTd"  >{el["h" + (index + 1)] && el["h" + (index + 1)].length > 0 ? el["h" + (index + 1)] === user.username ?
                                                <b className="tableBCalncle"
                                                    onClick={(e) => {
                                                        e.preventDefault();
                                                        if (type === "xbody") {
                                                            functions.deleteXbodyHour(el.id, `h${(index + 1)}`, token, addToast, id, setTrainers, setSchedule, setClubInfo)

                                                        } 
                                                        
                                                        if(type === "gym"){
                                                            functions.deleteGymHour(el.id, `h${(index + 1)}`, token, addToast, id, setSchedule)

                                                        }


                                                    }}>Отмени</b>
                                                : <b className="tableBBusy" >Заето</b>
                                                : <b className="tableBAvailable"
                                                    onClick={(e) => {
                                                        e.preventDefault();

                                                        if (type === "xbody") {
                                                            functions.reserveXbodyHour(el.id, `h${(index + 1)}`, token, t, user,addToast, id, setTrainers, setSchedule, setClubInfo)

                                                        } 
                                                        if(type === "gym") {
                                                            functions.reserveGymHour(el.id, `h${(index + 1)}`, token, addToast, id, setSchedule)

                                                        }
                                                    }}>Запази</b>}</td>
                                                :
                                                <td key={"value" + i} className="tableTd">{el["h" + (index + 1)] && el["h" + (index + 1)].length > 0
                                                    ? <b className="tableBBusy">Заето</b>
                                                    : <Link to="/login" className="tableBAvailable"><b >Запази</b></Link>}</td>
                                        )
                                    })}
                                </tr>
                            )
                        })}
                    </tbody>
                </Table>
                {user ? null : <div className="notUsetTitle">За да запазите своя час, трябва да <Link to="/login"><b className="notUsetTitleBold" style={{ "color": "#b9962da4", "fontFamily": "Oswald-Medium" }} >влезнете</b></Link> в профилa си.</div>
                }
                <br></br>
                <div style={{ "justifyContent": "center", "display": "grid" }}>
                    <Pagination
                        postsPerPage={postsPerPage}
                        totalPosts={schedulerTest.length}
                        paginate={paginate}
                        currentPage={currentPage}
                        style={{ "margin": "auto" }}
                    ></Pagination>
                </div>



            </div>
        </div>
    )
}

export default ScheduleXbody;

