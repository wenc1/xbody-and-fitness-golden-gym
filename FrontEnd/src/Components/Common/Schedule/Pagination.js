

const Pagination = ({ postsPerPage, totalPosts, paginate, currentPage }) => {
    const pageNumbers = [];

    for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
        pageNumbers.push(i);
    }

    return (
        <nav>
            <ul className='pagination'>
                {pageNumbers.map(number => (
                    <li key={number} className='page-item'>
                        {currentPage === number ?
                            <div style={{ "backgroundColor": "#b9962da4", "borderColor": "#b9962da4", "cursor": "pointer" }} onClick={(e) => { e.preventDefault(); paginate(number); }} href='!#' className='page-link'>
                                <b style={{ "color": "white" }}>{number}</b>
                            </div>
                            : <div style={{ "backgroundColor": "#50505040", "borderColor": "#b9962da4", "cursor": "pointer" }} onClick={(e) => { e.preventDefault(); paginate(number); }} href='!#' className='page-link'>
                                <b style={{ "color": "white" }}>{number}</b>
                            </div>}
                    </li>
                ))}
            </ul>
        </nav>
    );
};

export default Pagination;