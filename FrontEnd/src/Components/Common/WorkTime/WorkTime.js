const WorkTime = ({ times }) => {
    const hours = times.map((h) => {
        return (
            <div key={h.label}>{h.label} <br></br> <b className="singlePrice" style={{ "color": "#b9962da4" }}>{h.hours}</b> </div>
        )
    })
    return (
        <div className="pageElement">
            <div className="pricesXbody" style={{ "color": "white", "fontSize": "24px", "textAlign": "center", "fontStyle": "italic", "fontFamily": "Oswald-Medium" }}>
                {hours}
            </div>
        </div>
    )
}

export default WorkTime