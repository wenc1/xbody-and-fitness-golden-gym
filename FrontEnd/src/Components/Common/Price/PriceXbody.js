import { returnPrices } from '../../../Constants/constants'

const PriceXbody = ({ name }) => {
    const pricesToJSX = returnPrices(name).map((price) => {
        return (
            <div key={name + price.label}>{price.label} - <b className="singlePrice" style={{ "color": "#b9962da4" }}>{price.price} лв.</b> </div>
        )
    })
    return (
        <div className="pageElement">
            <div className="pricesXbody" style={{ "color": "white", "fontSize": "24px", "textAlign": "center", "fontStyle": "italic", "fontFamily": "Oswald-Medium" }}>
                {pricesToJSX}
            </div>
        </div>
    )
}

export default PriceXbody