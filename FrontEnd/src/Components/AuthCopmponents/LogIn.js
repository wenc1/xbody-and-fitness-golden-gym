import React, { useContext, useState } from 'react';
import Form from 'react-bootstrap/Form';
import { Link } from 'react-router-dom';
import './LogIn.css';
import functions from '../../functions/functions'
import { useToasts } from 'react-toast-notifications'
import AuthContext, { extractUser } from "../../Providers/AuthContext";
import {
    required,
    maxLen,
    minLen,
    validate,
    
} from './../../Validators/validators.js'
import Footer from '../Footer/Footer';

const LogIn = (props) => {
    
    const { addToast } = useToasts()
    const {  setLoginState } = useContext(AuthContext);

    const [usernameControl, setUsernameControl] = useState({
        value: '',
        valid: false,
        validators: [required, minLen(1), maxLen(50)],
    });
    const [passwordControl, setPasswordControl] = useState({
        value: '',
        valid: false,
        validators: [required, minLen(1), maxLen(30)],
    });
    // const [loading, setLoading] = useState(false);

    const onInputChange = (ev) => {
        const { value, name } = ev.target;

        if (name === 'username') {
            const copyControl = { ...usernameControl };
            copyControl.value = value;
            copyControl.valid = validate(value, usernameControl.validators);
            setUsernameControl(copyControl);
        } else if (name === 'password') {
            const copyControl = { ...passwordControl };
            copyControl.value = value;
            copyControl.valid = validate(value, passwordControl.validators);
            setPasswordControl(copyControl);
        }
    };

    console.log(passwordControl.value)
    return (
        <div style={{ "marginTop": "80px","position":"absolute","width":"100%" }}>
            <div style={{ "textAlign": "center", }}>
                <h5 className="titleLogin" style={{ "color": "#505050", "fontFamily": "Oswald-Medium" }}>Вход</h5>
            </div>
            <hr className="titleHr"></hr>
            <Form style={{ "textAlign": "center" }}>
                <Form.Group controlId='formBasicUsername' >
                    <h5 style={{ "color": "#505050", "fontFamily": "Oswald-Medium" }}>Потребителско име, Имейл или Тел. номер</h5>
                    <div className='inputLogin'>
                        <div>
                            <Form.Control
                                className='mr-sm-5'
                                value={usernameControl.value}
                                onChange={onInputChange}
                                name='username'
                                type='text'
                                placeholder='Въведи потребителско име'
                                isValid={usernameControl.value === '' ? null : !usernameControl.valid ? false : true}
                                isInvalid={usernameControl.value === '' ? false : usernameControl.valid ? false : true}
                            />
                        </div>
                    </div>
                    {/* <Form.Text className='text-muted'>Полето е задължително.</Form.Text> */}
                </Form.Group>

                <Form.Group controlId='formBasicPassword' >
                    <h5 style={{ "color": "#505050", "fontFamily": "Oswald-Medium" }}>Парола</h5>
                    <div className='inputLogin'>
                        <div>
                            <Form.Control
                                className='mr-sm-5'
                                value={passwordControl.value}
                                onChange={onInputChange}
                                name='password'
                                type='password'
                                placeholder='Въведи парола'
                                isValid={passwordControl.value === '' ? null : !passwordControl.valid ? false : true}
                                isInvalid={passwordControl.value === '' ? false : passwordControl.valid ? false : true}
                            />
                        </div>
                    </div>
                    {/* <Form.Text className='text-muted'>Полето е задължително.</Form.Text> */}

                    <div style={{"marginTop":"10px"}}>
                        <Link to="/reset" 
                        style={{ "color": "#505050" }}>Забравена парола?</Link>     
                                       </div>
                  
                    <br />
                    <button
                        className='btn'
                        disabled={
                            !usernameControl.valid ||
                            !passwordControl.valid
                        }
                        onClick={(e) => {
                            e.preventDefault()
                            functions.logIn(usernameControl.value, passwordControl.value,setLoginState, extractUser, addToast, props)
                        }}
                        style={{ "backgroundColor": "#b9962da4" }}
                    >
                        Login
        </button>
                    <div style={{"marginTop":"10px"}}>
                        <div style={{ "color": "#505050" }}>Нямаш акаунт? </div >
                        <Link to="/register" style={{ "color": "#77601d" }}> <b>Регистрирай се!</b></Link>
                    </div>
                </Form.Group>
            </Form>
            <Footer></Footer>
        </div>
    )
}

export default LogIn;