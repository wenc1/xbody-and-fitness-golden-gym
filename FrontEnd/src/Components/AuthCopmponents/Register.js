import React, {  useState } from 'react';
import Form from 'react-bootstrap/Form';
import './LogIn.css';
import { useToasts } from 'react-toast-notifications'
import functions from '../../functions/functions'
import {
    required,
    maxLen,
    minLen,
    emailValidator,
    phoneValidator,
    samePassword,
    validate,
    validatePass,
    hasWhiteSpace,usernameValidator
} from './../../Validators/validators.js'
import Footer from '../Footer/Footer';

const Register = (props) => {
    const { addToast } = useToasts()
 
    const [usernameControl, setUsernameControl] = useState({
        value: '',
        valid: false,
        validators: [required,usernameValidator, minLen(5), maxLen(15),hasWhiteSpace],
    });
    const [emailControl, setEmailControl] = useState({
        value: '',
        valid: false,
        validators: [required, minLen(5), maxLen(50), emailValidator,hasWhiteSpace],
    });
    const [firstNameControl, setFirstNameControl] = useState({
        value: '',
        valid: false,
        validators: [required, minLen(2), maxLen(30)],
    });
    const [lastNameControl, setLastNameControl] = useState({
        value: '',
        valid: false,
        validators: [required, minLen(2), maxLen(30)],
    });
    const [phoneControl, setPhoneControl] = useState({
        value: '',
        valid: false,
        validators: [required, phoneValidator,hasWhiteSpace],
    });
    const [passwordControl, setPasswordControl] = useState({
        value: '',
        valid: false,
        validators: [required, minLen(5), maxLen(30),hasWhiteSpace],
    });
    const [comfirmPasswordControl, setComfirmPasswordControl] = useState({
        value: '',
        valid: false,
        validators: [samePassword()],
    });


    // const [loading, setLoading] = useState(false);



    const onInputChange = (ev) => {
        const { value, name } = ev.target;

        if (name === 'username') {
            const copyControl = { ...usernameControl };
            copyControl.value = value;
            copyControl.valid = validate(value, usernameControl.validators);
            setUsernameControl(copyControl);
        } else if (name === 'password') {
            const copyControl = { ...passwordControl };
            copyControl.value = value;
            copyControl.valid = validate(value, passwordControl.validators);
            setPasswordControl(copyControl);
        } else if (name === 'email') {
            const copyControl = { ...emailControl };
            copyControl.value = value;
            copyControl.valid = validate(value, emailControl.validators);
            setEmailControl(copyControl);
        } else if (name === 'firstName') {
            const copyControl = { ...firstNameControl };
            copyControl.value = value;
            copyControl.valid = validate(value, firstNameControl.validators);
            setFirstNameControl(copyControl);
        } else if (name === 'lastName') {
            const copyControl = { ...lastNameControl };
            copyControl.value = value;
            copyControl.valid = validate(value, lastNameControl.validators);
            setLastNameControl(copyControl);
        } else if (name === 'phone') {
            const copyControl = { ...phoneControl };
            copyControl.value = value;
            copyControl.valid = validate(value, phoneControl.validators);
            setPhoneControl(copyControl);
        } else if (name === 'comfirmPassword') {
            const copyControl = { ...comfirmPasswordControl };
            copyControl.value = value;
            copyControl.valid = copyControl.valid = validatePass(
                value,
                passwordControl.value,
                comfirmPasswordControl.validators
              );
        
            setComfirmPasswordControl(copyControl);
        }
    };


    return (
        <div style={{ "marginTop": "80px","position":"absolute","width":"100%" }}>
            <div style={{ "textAlign": "center" }}>
                <h5 className="titleLogin" style={{ "color": "#505050", "fontFamily": "Oswald-Medium" }}>Регистрация</h5>
            </div>
            <hr className="titleHr"></hr>

            <Form style={{ "textAlign": "center" }}>
                <Form.Group controlId='username' md={6} >
                    <h5 style={{ "color": "#505050", "fontFamily": "Oswald-Medium" }}>Потребителско име</h5>
                    <div className='inputLogin'>
                        <div>
                            <Form.Control
                                className='mr-sm-5'
                                value={usernameControl.value}
                                onChange={onInputChange}
                                name='username'
                                type='text'
                                placeholder='Въведи потребителско име'
                                isValid={usernameControl.value === '' ? null : !usernameControl.valid ? false : true}
                                isInvalid={usernameControl.value === '' ? false : usernameControl.valid ? false : true}
                            />
                        </div>
                    </div>
                    <Form.Text className='text-muted'>Портребителското име трябва да бъде текст с дължина между 5 и 15 символа.</Form.Text>

                </Form.Group>
                <Form.Group controlId='email' >
                    <h5 style={{ "color": "#505050", "fontFamily": "Oswald-Medium" }}>Имейл</h5>
                    <div className='inputLogin'>
                        <div>
                            <Form.Control
                                className='mr-sm-5'
                                value={emailControl.value}
                                onChange={onInputChange}
                                name='email'
                                type='text'
                                placeholder='Въведи имейл'
                                isValid={emailControl.value === '' ? null : !emailControl.valid ? false : true}
                                isInvalid={emailControl.value === '' ? false : emailControl.valid ? false : true}
                            />
                        </div>
                    </div>
                    <Form.Text className='text-muted'>Имейлът трябва да бъде валиден.</Form.Text>
                </Form.Group>
                <Form.Group controlId='firstname' >
                    <h5 style={{ "color": "#505050", "fontFamily": "Oswald-Medium" }}>Име</h5>
                    <div className='inputLogin'>
                        <div>
                            <Form.Control
                                className='mr-sm-5'
                                value={firstNameControl.value}
                                onChange={onInputChange}
                                name='firstName'
                                type='text'
                                placeholder='Въведи първо име'
                                isValid={firstNameControl.value === '' ? null : !firstNameControl.valid ? false : true}
                                isInvalid={firstNameControl.value === '' ? false : firstNameControl.valid ? false : true}
                            />
                        </div>
                    </div>
                    <Form.Text className='text-muted'>Полето е задължително.</Form.Text>
                </Form.Group>
                <Form.Group controlId='lastname' >
                    <h5 style={{ "color": "#505050", "fontFamily": "Oswald-Medium" }}>Фамилия</h5>
                    <div className='inputLogin'>
                        <div>
                            <Form.Control
                                className='mr-sm-5'
                                value={lastNameControl.value}
                                onChange={onInputChange}
                                name='lastName'
                                type='text'
                                placeholder='Въведи фамилия'
                                isValid={lastNameControl.value === '' ? null : !lastNameControl.valid ? false : true}
                                isInvalid={lastNameControl.value === '' ? false : lastNameControl.valid ? false : true}
                            />
                        </div>
                    </div>
                    <Form.Text className='text-muted'>Полето е задължително.</Form.Text>
                </Form.Group>
                <Form.Group controlId='phone' >
                    <h5 style={{ "color": "#505050", "fontFamily": "Oswald-Medium" }}>Телефонен номер</h5>
                    <div className='inputLogin'>
                        <div>
                            <Form.Control
                                className='mr-sm-5'
                                value={phoneControl.value}
                                onChange={onInputChange}
                                name='phone'
                                type='text'
                                placeholder='Въведи тел. номер'
                                isValid={phoneControl.value === '' ? null : !phoneControl.valid ? false : true}
                                isInvalid={phoneControl.value === '' ? false : phoneControl.valid ? false : true}
                            />
                        </div>
                    </div>
                    <Form.Text className='text-muted'>Телефонният номер трябва да бъде валиден (+3598********).</Form.Text>
                </Form.Group>
                <Form.Group controlId='password' >
                    <h5 style={{ "color": "#505050", "fontFamily": "Oswald-Medium" }}>Парола</h5>
                    <div className='inputLogin'>
                        <div>
                            <Form.Control
                                className='mr-sm-5'
                                value={passwordControl.value}
                                onChange={onInputChange}
                                name='password'
                                type='password'
                                placeholder='Въведи парола'
                                isValid={passwordControl.value === '' ? null : !passwordControl.valid ? false : true}
                                isInvalid={passwordControl.value === '' ? false : passwordControl.valid ? false : true}
                            />
                        </div>
                    </div>
                    <Form.Text className='text-muted'>Парлата трябва да бъде текст с дължина между 5 и 30 символа.</Form.Text>
                    <br />
                    <Form.Group controlId='confirmpassword' >
                    <h5 style={{ "color": "#505050", "fontFamily": "Oswald-Medium" }}>Повтори парола</h5>
                    <div className='inputLogin'>
                        <div>
                            <Form.Control
                                className='mr-sm-5'
                                value={comfirmPasswordControl.value}
                                onChange={onInputChange}
                                name='comfirmPassword'
                                type='password'
                                placeholder='Повтори парола'
                                isValid={comfirmPasswordControl.value === '' ? null : !comfirmPasswordControl.valid ? false : true}
                                isInvalid={comfirmPasswordControl.value === '' ? false : comfirmPasswordControl.valid ? false : true}
                            />
                        </div>
                    </div>
                    <Form.Text className='text-muted'>Паролата трябва да съвпада с подадената по-горе.</Form.Text>
                    </Form.Group>


                    <br />
                    <button
                        className='btn'
                        disabled={
                            !usernameControl.valid ||
                            !passwordControl.valid ||
                            !comfirmPasswordControl.valid ||
                            !emailControl.valid ||
                            !firstNameControl.valid ||
                            !lastNameControl.valid ||
                            !phoneControl.valid
                        }
                        onClick={(e) => {
                            e.preventDefault()
                            functions.register(usernameControl.value, emailControl.value,firstNameControl.value,lastNameControl.value,phoneControl.value,passwordControl.value,props,addToast)
                        }}
                        style={{ "backgroundColor": "#b9962da4" }}
                    >
                        Регистрация
                    </button>
                </Form.Group>
            </Form>
            <Footer></Footer>

        </div>
    )
}

export default Register;