import React, {  useState } from 'react';
import Form from 'react-bootstrap/Form';
import './LogIn.css';
import emailjs from "emailjs-com";
import functions from '../../functions/functions';
import { useToasts } from 'react-toast-notifications'
import {
    required,
    maxLen,
    minLen,
    emailValidator,
    samePassword,
    validate,
    validatePass
} from './../../Validators/validators.js'
import Footer from '../Footer/Footer';

const ResetPassword = (props) => {
    const [emailControl, setEmailControl] = useState({
        value: '',
        valid: false,
        validators: [required, minLen(5), maxLen(50), emailValidator],
    });
    const [passwordControl, setPasswordControl] = useState({
        value: '',
        valid: false,
        validators: [required, minLen(5), maxLen(30)],
    });
    const [comfirmPasswordControl, setComfirmPasswordControl] = useState({
        value: '',
        valid: false,
        validators: [samePassword()],
    });
    const [keyControl, setKeyControl] = useState({
        value: '',
        valid: false,
        validators: [required],
    });

    const { addToast } = useToasts()


    // const [loading, setLoading] = useState(false);


    const onInputChange = (ev) => {
        const { value, name } = ev.target;

        if (name === 'password') {
            const copyControl = { ...passwordControl };
            copyControl.value = value;
            copyControl.valid = validate(value, passwordControl.validators);
            setPasswordControl(copyControl);
        } else if (name === 'email') {
            const copyControl = { ...emailControl };
            copyControl.value = value;
            copyControl.valid = validate(value, emailControl.validators);
            setEmailControl(copyControl);
        } else if (name === 'comfirmPassword') {
            const copyControl = { ...comfirmPasswordControl };
            copyControl.value = value;
            copyControl.valid = copyControl.valid = validatePass(
                value,
                passwordControl.value,
                comfirmPasswordControl.validators
            );

            setComfirmPasswordControl(copyControl);
        }
        else if (name === 'key') {
            const copyControl = { ...keyControl };
            copyControl.value = value;
            copyControl.valid = validate(value, keyControl.validators);
            setKeyControl(copyControl);
        }
    };


    return (
        <div style={{ "marginTop": "80px", "position": "absolute","width":"100%" }}>
            <div style={{ "textAlign": "center" }}>
                <h5 className="titleLogin" style={{ "color": "#505050", "fontFamily": "Oswald-Medium" }}>Възстановяване на парола</h5>
            </div>
            <hr className="titleHr"></hr>
            <Form style={{ "textAlign": "center" }}>
                <Form.Group controlId='email' >
                    <h5 style={{ "color": "#505050", "fontFamily": "Oswald-Medium" }}>Имейл</h5>
                    <div className='inputLogin'>
                        <div>
                            <Form.Control
                                className='mr-sm-5'
                                value={emailControl.value}
                                onChange={onInputChange}
                                name='email'
                                type='text'
                                placeholder='Въведи имейл'
                                isValid={emailControl.value === '' ? null : !emailControl.valid ? false : true}
                                isInvalid={emailControl.value === '' ? false : emailControl.valid ? false : true}
                            />
                        </div>
                    </div>
                    <Form.Text className='text-muted'></Form.Text>
                </Form.Group>
                <Form.Group controlId='key' >
                    <h5 style={{ "color": "#505050", "fontFamily": "Oswald-Medium" }}>Ключ</h5>
                    <div className='inputLogin'>
                        <div>
                            <Form.Control
                                className='mr-sm-5'
                                value={keyControl.value}
                                onChange={onInputChange}
                                name='key'
                                type='text'
                                placeholder='Въведи ключ'
                                isValid={keyControl.value === '' ? null : !keyControl.valid ? false : true}
                                isInvalid={keyControl.value === '' ? false : keyControl.valid ? false : true}
                            />
                        </div>
                    </div>
                    <Form.Text
                        className='text-muted'
                        style={{ "cursor": "pointer" }}
                        onClick={(e) => {
                            e.preventDefault()
                            functions.sentEmail(emailControl.value, addToast, emailjs)
                        }}> Получи ключ по имейл.</Form.Text>
                </Form.Group>

                <Form.Group controlId='password' >
                    <h5 style={{ "color": "#505050", "fontFamily": "Oswald-Medium" }}>Нова парола</h5>
                    <div className='inputLogin'>
                        <div>
                            <Form.Control
                                className='mr-sm-5'
                                value={passwordControl.value}
                                onChange={onInputChange}
                                name='password'
                                type='password'
                                placeholder='Въведи нова парола'
                                isValid={passwordControl.value === '' ? null : !passwordControl.valid ? false : true}
                                isInvalid={passwordControl.value === '' ? false : passwordControl.valid ? false : true}
                            />
                        </div>
                    </div>
                    <Form.Text className='text-muted'>Парлата трябва да бъде текст с дължина между 5 и 30 символа.</Form.Text>

                    <br />
                    <Form.Group controlId='comfirmPassword' >
                        <h5 style={{ "color": "#505050", "fontFamily": "Oswald-Medium" }}>Повтори парола</h5>
                        <div className='inputLogin'>
                            <div>
                                <Form.Control
                                    className='mr-sm-5'
                                    value={comfirmPasswordControl.value}
                                    onChange={onInputChange}
                                    name='comfirmPassword'
                                    type='password'
                                    placeholder='Повтори новата парола'
                                    isValid={comfirmPasswordControl.value === '' ? null : !comfirmPasswordControl.valid ? false : true}
                                    isInvalid={comfirmPasswordControl.value === '' ? false : comfirmPasswordControl.valid ? false : true}
                                />
                            </div>
                        </div>
                        <Form.Text className='text-muted'>Паролата трябва да съвпада с подадената по-горе.</Form.Text>
                    </Form.Group>

                    <br />
                    <button
                        className='btn'
                        disabled={
                            !passwordControl.valid ||
                            !comfirmPasswordControl.valid ||
                            !emailControl.valid ||
                            !keyControl.valid
                        }
                        onClick={(e) => {
                            e.preventDefault()
                            functions.changePassword(props, emailControl.value, passwordControl.value, keyControl.value, addToast)
                        }}
                        style={{ "backgroundColor": "#b9962da4" }}
                    >
                        Смяна
                    </button>
                </Form.Group>
            </Form>
            <Footer></Footer>

        </div>
    )
}

export default ResetPassword;