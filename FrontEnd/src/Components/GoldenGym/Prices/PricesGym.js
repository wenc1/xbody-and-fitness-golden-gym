import { useEffect, useState } from "react"
import { Button, Spinner } from "react-bootstrap"
import { returnGymPrices } from "../../../Constants/constants"

const PricesGym = ({ typePrices, setTypePrices }) => {
    const [prices, setPrices] = useState(returnGymPrices("Студенти"))
    const [loader, setLoader] = useState(true)
    useEffect(() => {
        setLoader(true)
        setPrices(returnGymPrices(typePrices))
        setLoader(false)
    }, [typePrices])

    const pricesToJSX = prices.map((price) => {
        return (
            <div key={typePrices + price.label}>{price.label} - <b className="singlePrice" style={{ "color": "#b9962da4" }}>{price.price} лв.</b> </div>
        )
    })
    return (
        <div className="pageElement">
            <div style={{ "display": "flex" }}>
                <div style={{ "width": "33%", "justifyContent": "center", "display": "flex" }}>
                    {typePrices === "Ученици" ?
                        <Button variant="warning" disabled style={{ "backgroundColor": "#b9962da4", "borderColor": "#b9962da4", "color": "white" }}>Ученици</Button>
                        :
                        <Button variant="warning" style={{ "backgroundColor": "#b9962da4", "borderColor": "#b9962da4", "color": "white" }} onClick={() => { setTypePrices("Ученици") }}>Ученици</Button>
                    }
                </div>
                <div style={{ "width": "33%", "justifyContent": "center", "display": "flex" }}>
                    {typePrices === "Студенти" ?
                        <Button variant="warning" disabled style={{ "backgroundColor": "#b9962da4", "borderColor": "#b9962da4", "color": "white" }}>Студенти</Button>
                        :
                        <Button variant="warning" style={{ "backgroundColor": "#b9962da4", "borderColor": "#b9962da4", "color": "white" }} onClick={() => { setTypePrices("Студенти") }}>Студенти</Button>
                    }

                </div>
                <div style={{ "width": "33%", "justifyContent": "center", "display": "flex" }}>
                    {typePrices === "Възрастни" ?
                        <Button variant="warning" disabled style={{ "backgroundColor": "#b9962da4", "borderColor": "#b9962da4", "color": "white" }}>Възрастни</Button>
                        :
                        <Button variant="warning" style={{ "backgroundColor": "#b9962da4", "borderColor": "#b9962da4", "color": "white" }} onClick={() => { setTypePrices("Възрастни") }}>Възрастни</Button>
                    }
                </div>
            </div>
            <br></br>
            <div className="pageElement">
                <h2 key={"prices"+typePrices} className="subTitleSingleXbodyText" style={{"fontSize":"28px"}}>Цени за {typePrices}</h2>
                <hr className="subTitleSingleXbodyHr" style={{ "width": "50%" }}></hr>
                {!loader ?
                    <div className="pricesXbody" style={{ "color": "white", "fontSize": "24px", "textAlign": "center", "fontStyle": "italic", "fontFamily": "Oswald-Medium" }}>
                        {pricesToJSX}
                    </div> :
                    <div style={{ "textAlign": "center" }}>
                        <Spinner animation="border" style={{ "width": "150px", "height": "150px", "color": "#b9962da4", "marginTop": "20px", "marginBottom": "20px" }}></Spinner>
                    </div>}

            </div>
        </div>
    )
}

export default PricesGym