import { Col, Container, Row } from "react-bootstrap"
import { Link } from "react-router-dom"
import { url } from "../../../Constants/constants"

const PersonalTrainersCards = ({ trainers }) => {

    
    const trainersView = trainers.filter((trainer) => trainer.position === "Треньор" ).map((trainer) => {
 
            const nav = "/gym/trainer/" + trainer.id;
            return (

                <Col lg={4} xs={9} sm={6} md={4} key={"trainer" + trainer.id + trainer.name + "personal"}>
                    <Link to={nav}>
                        <div className="singleTrainer">
                            <figure className="team-photo">
                                {trainer.photo ?
                                    <img className="profile-picture" src={`${url}/public/` + trainer.photo} alt=""></img>
                                    : <img className="profile-picture" src="https://www.shareicon.net/data/2016/07/05/791224_man_512x512.png" alt=""></img>}
                            </figure>

                            {/* <h3 className="trainer-name" style={{ "fontFamily": "Oswald-Medium" }} >{trainer.name}</h3> */}
                            {/* <br></br> */}
                            <h4 className="trainer-name" style={{ "fontFamily": "Oswald-Medium" }} >Тренирай с {trainer.name}</h4>
                        </div>
                    </Link>
                </Col>
            )
        

    })


    return (
        <div className="pageElement">

            <Container>
                <Row style={{ "justifyContent": "center" }}>
                    {trainersView}
                </Row>
            </Container>
        </div>
    )
}

export default PersonalTrainersCards;