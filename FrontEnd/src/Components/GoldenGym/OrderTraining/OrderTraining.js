import { useContext, useState } from "react";
import { Button, Col, Form, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import { questionFive, questionFour, questionOne, questionSeven, questionSix, questionThree, questionTwo, baseQuestion } from "../../../Constants/constants";
import functions from "../../../functions/functions";
import AuthContext from "../../../Providers/AuthContext";
import { minLen, numberIntValidator, numberValidator, required, validate } from "../../../Validators/validators";
import emailjs from "emailjs-com";
import { useToasts } from "react-toast-notifications";
import SubTitle from "../../Common/SubTitle/SubTitle";

const OrderTraining = ({ trainer }) => {
    const { user } = useContext(AuthContext);
    const { addToast } = useToasts()
    const [height, setHeight] = useState({
        value: '',
        valid: false,
        validators: [required, numberValidator],
    });

    const [kilograms, setKilograms] = useState({
        value: '',
        valid: false,
        validators: [required, numberValidator],
    });

    const [biceps, setBiceps] = useState({
        value: '',
        valid: false,
        validators: [required, numberValidator],
    });
    const [chest, setChest] = useState({
        value: '',
        valid: false,
        validators: [required, numberValidator],
    });

    const [forearm, setForearm] = useState({
        value: '',
        valid: false,
        validators: [required, numberValidator],
    });
    const [bodies, setBodies] = useState({
        value: '',
        valid: false,
        validators: [required, numberValidator],
    });
    const [hip, setHip] = useState({
        value: '',
        valid: false,
        validators: [required, numberValidator],
    });
    const [thigh, setThigh] = useState({
        value: '',
        valid: false,
        validators: [required, numberValidator],
    });
    const [calf, setCalf] = useState({
        value: '',
        valid: false,
        validators: [required, numberValidator],
    });
    const [goals, setGoals] = useState({
        value: '',
        valid: false,
        validators: [required, minLen(1)],
    });
    const [weeklyT, setWeeklyT] = useState({
        value: '',
        valid: false,
        validators: [required, numberIntValidator],
    });
    const [qBaseAnswer, setQBaseAnswer] = useState(null);
    const [qOneAnswer, setQOneAnswer] = useState(null);
    const [qTwoAnswer, setQTwoAnswer] = useState([]);
    const [qThreeAnswer, setQThreeAnswer] = useState(null);
    const [qFourAnswer, setQFourAnswer] = useState([]);
    const [qFiveAnswer, setQFiveAnswer] = useState([]);
    const [qSixAnswer, setQSixAnswer] = useState([]);
    const [qSevenAnswer, setQSevenAnswer] = useState([]);
    const [isOpen, setIsOpen] = useState(false);

    const onChangeCheckBox = (target, arr, setArr) => {
        if (arr.includes(target)) {
            const newArr = arr.filter(el => el !== target).map(el => {
                    return el
            })

            setArr([...newArr])
        } else {
            setArr([...arr, target])
        }
    }


    const CheckBox = (data, label, arr, setArr) => {
        return (

            <Col style={{ "marginBottom": "5%" }}>
                {/* <Form> */}
                <Form.Label><b style={{ "color": "#505050", "fontFamily": "Oswald-Medium", "fontSize": "22px" }}>{label}</b></Form.Label>
                {data.map((q, index) => {
                    return (
                        <Form.Check style={{ "color": "white" }}
                            key={q + index}
                            type="checkbox"
                            label={q}
                            value={q}
                            onChange={(ev) => { onChangeCheckBox(ev.target.value, arr, setArr) }}
                            id={q + index}
                        />
                    )
                })}
                {/* </Form> */}
            </Col>
        )
    }

    const Radio = (data, label, setValue) => {
        return (
            <Col style={{ "marginBottom": "5%" }}>
                {/* <Form> */}
                <Form.Label><b style={{ "color": "#505050", "fontFamily": "Oswald-Medium", "fontSize": "22px" }}>{label}</b></Form.Label>
                {data.map((q, index) => {
                    return (
                        <Form.Check
                            style={{ "color": "white" }}
                            key={q + index}
                            type="radio"
                            name="radioButton"
                            label={q}
                            onChange={(ev) => { setValue(ev.target.value) }}
                            value={q}
                            id={q + index}
                        />
                    )
                })}
                {/* </Form> */}
            </Col>
        )
    }

    const CustomInput = (label, type, placeholder, id, name, eValue, setEValue, ft) => {
        return (
            <Form.Group as={Col} controlId={id}>
                <Form.Label><b style={{ "color": "#505050", "fontFamily": "Oswald-Medium", "fontSize": "22px" }}>{label}</b></Form.Label>
                <Form.Control
                    type={type}
                    placeholder={placeholder}
                    onChange={(ev) => { onInputChange(ev, eValue, setEValue) }}
                    isValid={eValue.value === '' ? null : !eValue.valid ? false : true}
                    isInvalid={eValue.value === '' ? false : eValue.valid ? false : true}
                    name={name} />
                {ft ?
                    <Form.Text className="text-muted">
                        Размерите трябва да бъдат в сантиметри.</Form.Text> : null}
            </Form.Group>
        )
    }

    const onInputChange = (ev, eValue, eSetValue) => {
        const { value } = ev.target;
        const copyControl = { ...eValue };
        copyControl.value = value;
        copyControl.valid = validate(value, eValue.validators);
        eSetValue(copyControl);
    }

    return (
        <div className="pageElement" style={{ "width": "80%", "marginLeft": "10%" }}>
            {isOpen ?
                <>
                    <SubTitle text={"Попълни анкетата"}></SubTitle>

                    <Form>
                        <Form.Row>
                            {Radio(baseQuestion, "Какво желаете да поръчате?", setQBaseAnswer)}

                        </Form.Row>
                        <Form.Row>
                            {CustomInput("Ръст", "text", " ", "РъстФорма", "height", height, setHeight, true)}
                            {CustomInput("Килограми", "text", " ", "КилограмиФорма", "kilograms", kilograms, setKilograms, true)}
                        </Form.Row>

                        <Form.Row>
                            {CustomInput("Бицепс", "text", " ", "БицепсФорма", "biceps", biceps, setBiceps, true)}
                            {CustomInput("Гръдна Обиколка", "text", " ", "ГръднаОбиколка", "chest", chest, setChest, true)}
                        </Form.Row>


                        <Form.Row>
                            {CustomInput("Предмишница", "text", " ", "ПредмишницаФорма", "forearm", forearm, setForearm, true)}
                            {CustomInput("Талия", "text", " ", "ТалияФорма", "bodies", bodies, setBodies, true)}
                        </Form.Row>

                        <Form.Row>
                            {CustomInput("Ханш", "text", " ", "ХаншФорма", "thigh", thigh, setThigh, true)}
                            {CustomInput("Бедро", "text", " ", "БедроФорма", "hip", hip, setHip, true)}
                            {CustomInput("Прасец", "text", " ", "ПрасецФорма", "calf", calf, setCalf, true)}
                        </Form.Row>

                        <Form.Row>
                            {CustomInput("Цели", "text", " ", "ЦелиФорма", "goals", goals, setGoals, false)}
                        </Form.Row>

                        <Form.Row>
                            {CustomInput("Тренировки седмично", "text", "", "Тренировки", "weeklyT", weeklyT, setWeeklyT, true)}
                        </Form.Row>
                    </Form>

                    <Row>
                        {Radio(questionOne, "Определете вашата физическа активност?", setQOneAnswer)}
                        {CheckBox(questionTwo, "Кое от изброените е вярно за вас?", qTwoAnswer, setQTwoAnswer)}
                        {Radio(questionThree, "Колко пъти се храните на ден?", setQThreeAnswer)}
                    </Row>

                    <Row>
                        {CheckBox(questionFour, "Какво месо консумирате?", qFourAnswer, setQFourAnswer)}
                        {CheckBox(questionFive, "Какви зеленчуци и зърнени храни консумирате?", qFiveAnswer, setQFiveAnswer)}
                        {CheckBox(questionSix, "Какви продукти консумирате?", qSixAnswer, setQSixAnswer)}
                        {CheckBox(questionSeven, "Какви плодове консумирате?", qSevenAnswer, setQSevenAnswer)}
                    </Row>
                    <div style={{ "display": "grid" }}>
                        {qBaseAnswer && height.valid && kilograms.valid &&
                            biceps.valid && chest.valid && forearm.valid && bodies.valid &&
                            hip.valid && thigh.valid && calf.valid && goals.valid && weeklyT.valid &&
                            qOneAnswer && qTwoAnswer.length > 0 && qThreeAnswer && qFourAnswer.length > 0 &&
                            qFiveAnswer.length > 0 && qSixAnswer.length > 0 && qSevenAnswer.length > 0
                            ?
                            <Button variant="warning" style={{ "backgroundColor": "#b9962da4", "borderColor": "#b9962da4", "color": "white", "margin": "auto" }} onClick={(e) => {
                                setQOneAnswer(setIsOpen(false))
                                const data = {
                                    f1: qBaseAnswer,
                                    f2: height.value,
                                    f3: kilograms.value,
                                    f4: biceps.value,
                                    f5: chest.value,
                                    f6: forearm.value,
                                    f7: bodies.value,
                                    f8: hip.value,
                                    f9: thigh.value,
                                    f10: calf.value,
                                    f11: goals.value,
                                    f12: weeklyT.value,
                                    q1: qOneAnswer,
                                    q2: qTwoAnswer.join(", "),
                                    q3: qThreeAnswer,
                                    q4: qFourAnswer.join(", "),
                                    q5: qFiveAnswer.join(", "),
                                    q6: qSixAnswer.join(", "),
                                    q7: qSevenAnswer.join(", "),

                                }
                                functions.sentTrainerEmail(addToast, emailjs, user, data, trainer);

                                setHeight(
                                    {
                                        value: '',
                                        valid: false,
                                        validators: [required, numberValidator],
                                    }
                                )
                                setKilograms(
                                    {
                                        value: '',
                                        valid: false,
                                        validators: [required, numberValidator],
                                    }
                                )

                                setBiceps(
                                    {
                                        value: '',
                                        valid: false,
                                        validators: [required, numberValidator],
                                    }
                                )

                                setChest(
                                    {
                                        value: '',
                                        valid: false,
                                        validators: [required, numberValidator],
                                    }
                                )

                                setForearm(
                                    {
                                        value: '',
                                        valid: false,
                                        validators: [required, numberValidator],
                                    }
                                )

                                setBodies(
                                    {
                                        value: '',
                                        valid: false,
                                        validators: [required, numberValidator],
                                    }
                                )

                                setHip(
                                    {
                                        value: '',
                                        valid: false,
                                        validators: [required, numberValidator],
                                    }
                                )

                                setThigh(
                                    {
                                        value: '',
                                        valid: false,
                                        validators: [required, numberValidator],
                                    }
                                )


                                setCalf(
                                    {
                                        value: '',
                                        valid: false,
                                        validators: [required, numberValidator],
                                    }
                                )


                                setGoals(
                                    {
                                        value: '',
                                        valid: false,
                                        validators: [required, minLen(1)]
                                    }
                                )

                                setWeeklyT(
                                    {
                                        value: '',
                                        valid: false,
                                        validators: [required, numberIntValidator]
                                    }
                                )

                                setQBaseAnswer(null);
                                setQOneAnswer(null);
                                setQTwoAnswer([]);
                                setQThreeAnswer(null);
                                setQFourAnswer([]);
                                setQFiveAnswer([]);
                                setQSixAnswer([]);
                                setQSevenAnswer([]);
                            }}>Завърши поръчка</Button>
                            :
                            <>
                                <Button variant="warning disabled" style={{ "backgroundColor": "#b9962da4", "borderColor": "#b9962da4", "color": "white", "margin": "auto", "marginBottom": "5px", "cursor": "none" }} >Завърши поръчка </Button>
                                <div className="notUsetTitle">За да заършите поръчката, всички полета трябва да бъдат попълнени с валидни данни.</div>
                            </>
                        }

                    </div>
                </> :
                <div style={{ "display": "grid" }}>
                    {user ?
                        <Button variant="warning" style={{ "backgroundColor": "#b9962da4", "borderColor": "#b9962da4", "color": "white", "margin": "auto" }} onClick={(e) => { e.preventDefault(); setIsOpen(true) }}>Поръчай сега</Button>

                        :
                        <>
                            <Button variant="warning disaled" style={{ "backgroundColor": "#b9962da4", "borderColor": "#b9962da4", "color": "white", "margin": "auto" }}>Поръчай сега</Button>
                            <div className="notUsetTitle" style={{"marginTop":"5px"}}>За да запазите своя час, трябва да <Link to="/login"><b className="notUsetTitleBold" style={{ "color": "#b9962da4", "fontFamily": "Oswald-Medium"}} >влезнете</b></Link> в профилa си.</div>
                        </>}
                </div>

            }
        </div >
    )
}

export default OrderTraining;