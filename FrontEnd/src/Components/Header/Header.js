import React, { useContext, useState } from 'react';
import Logo from './Logo2.png'
import './../../ThemeCSSFiles/css/main.css'
import './../../ThemeCSSFiles/css/media.css'
import './../../ThemeCSSFiles/css/fonts.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import AuthContext from '../../Providers/AuthContext';
import { Link } from 'react-router-dom';
import functions from '../../functions/functions';

const Header = (props) => {
    const [hiddenMenu, setHiddenMenu] = useState(false);
    const { isLoggedIn, setLoginState } = useContext(AuthContext);
    return (
        <div className="header" id="poligonal" >
            <span className="btn-menu" onClick={(e) => { e.preventDefault(); setHiddenMenu(!hiddenMenu) }}>
                <FontAwesomeIcon icon={faBars}></FontAwesomeIcon>
            </span>
            {hiddenMenu ? <nav className="hidden-menu" onClick={(e) => { e.preventDefault(); setHiddenMenu(!hiddenMenu) }}>
                <ul>
                    <li><Link to="/home"><b>Начало</b></Link></li>
                    <li><Link to="/xbody"><b>Xbody</b></Link></li>
                    <li><Link to="/gym"><b>Golden Gym</b></Link></li>
                    {isLoggedIn ? <li><Link to="/home" 
                    onClick={(e) => {
                         e.preventDefault(); 
                         functions.logOut(setLoginState) 
                        }}><b>Изход</b></Link></li>
                        : <> <li><Link to="/login"><b>Вход</b></Link></li>
                            <li><Link to="/register"><b>Регистрация</b></Link></li>
                        </>}
                </ul>
            </nav> : null}
            <header className="header-wrapperr">
                <div className="header-container">
                    <div className="header-logo" >
                        <Link to="/home">
                            <img src={Logo} alt="" ></img>
                        </Link>
                    </div>
                </div>
            </header>
        </div>
    )
}

export default Header