import { faFacebookF, faInstagram } from "@fortawesome/free-brands-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Link } from "react-router-dom"
import PhoneNumber from 'react-phone-number';
import GDPR from './Images/GDPR.png'
import './Footer.css'

const Footer = () => {
    return (
        <div style={{ "background": "#101010", "paddingTop": "15px", "paddingBottom": "15px", "marginTop": "40px" }}>
            <hr className="footerHr"
                style={{ "width": "80%", "margin": "auto", "height": "3px", "marginBottom": "15px", "backgroundColor": "#b9962da4" }}></hr>

            <div style={{ "display": "flex" }}>
                <div style={{ "width": "33%", "color": "whitesmoke", "textAlign": "center" }}>
                    <p style={{ "fontSize": "23px" }}>Други</p>
                    <p style={{ "width": "100%", "textAlign": "center", "fontSize": "14px" }} >
                        <Link to="/thermsandconditions" style={{ "color": "whitesmoke" }}>Общи условия</Link>
                    </p>
                    <p style={{ "width": "100%", "textAlign": "center" }}>
                        <Link to="/delivery" style={{ "color": "whitesmoke", "fontSize": "14px" }}>Доставка</Link>
                    </p>

                </div>
                <div style={{ "width": "33%", "color": "whitesmoke", "textAlign": "center" }}>
                    <p style={{ "fontSize": "23px" }}>Контакти</p>

                    <p style={{ "width": "100%", "textAlign": "center", "fontSize": "14px" }} >
                    XBODY София <br></br>

                        <PhoneNumber className="phone-number" number="+359892090908" isLinked={true} />

                    </p>
                    <p style={{ "width": "100%", "textAlign": "center", "fontSize": "14px","color":"whitesmoke" }} >
                    XBODY Благоевград <br></br>
                        <PhoneNumber className="phone-number" number="+359886630404" isLinked={true} style={{"color":"whitesmoke !important"}}/>

                    </p>
                    <p style={{ "width": "100%", "textAlign": "center", "fontSize": "14px" }} >
                       XBODY Пловдив <br></br>
                        <PhoneNumber className="phone-number" number="+359896440660" isLinked={true} />
                    </p>
                    <p style={{ "width": "100%", "textAlign": "center", "fontSize": "14px" }} >
                       Golden Gym <br></br>
                        <PhoneNumber className="phone-number" number="+359895891729" isLinked={true} />
                    </p>
                </div>
                <div style={{ "width": "33%", "color": "whitesmoke", "textAlign": "center" }}>
                    <p style={{ "fontSize": "23px" }}>Социални <br></br> мрежи</p>
                    <div style={{ "display": "flex", "width": "50%", "margin": "auto" }}>

                        <div style={{ "width": "50%", "textAlign": "center" }} >
                            <FontAwesomeIcon icon={faFacebookF} className="social-icon"></FontAwesomeIcon>
                        </div>
                        <div style={{ "width": "50%", "textAlign": "center" }}>
                            <FontAwesomeIcon icon={faInstagram} className="social-icon"></FontAwesomeIcon>
                        </div>
                    </div>
                </div>
            </div>
            <div style={{ "width": "80%", "textAlign": "center", "justifyContent": "center", "marginLeft": "10%", "marginTop": "15px", "fontSize": "15px" }}>
                <hr className="footerHr" style={{ "width": "100%", "margin": "auto", "height": "3px", "marginTop": "15px", "marginBottom": "15px", "backgroundColor": "#b9962da4" }}></hr>
                <p style={{ "width": "100%", "color": "whitesmoke", }}><img src={GDPR} alt="" style={{ "width": "40px", "height": "40px" }}></img> &nbsp;  Сайтът е на 100% съобразен с GDPR. <Link to="#" style={{ "color": "whitesmoke" }}><b>Нашите условия.</b></Link></p>
            </div>
        </div>
    )
}

export default Footer