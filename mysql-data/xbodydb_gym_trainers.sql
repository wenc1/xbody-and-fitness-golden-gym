-- MySQL dump 10.16  Distrib 10.1.38-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: xbodydb
-- ------------------------------------------------------
-- Server version	10.5.9-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gym_trainers`
--

DROP TABLE IF EXISTS `gym_trainers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gym_trainers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modal_photo` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(2056) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gym_trainers`
--

LOCK TABLES `gym_trainers` WRITE;
/*!40000 ALTER TABLE `gym_trainers` DISABLE KEYS */;
INSERT INTO `gym_trainers` VALUES (1,'Любомир  Николов','trainer1blagoevgrad.jpg','trainer1blagoevgradModal.jpg','l.nikolov.goldengymbg@mail.bg','Здравейте! Казвам се Любомир Николов на 24 години от Благоевград. Студент съм в Югозападен универистет “Неофит Рилски”, специалност “Право”. Занивам се със спор от ранна възраст. Спортувал съм: лека стлетика, бокс, плуване. С фитнес се занимавам от 16 годишен. Моят баща ме запали по фитнеса, като ми подари книга за бодибилдинг от неговото детство. Нещата бързо прерастнаха от, обикновенно ходене до фитнес залата, до сериозни тренировки и спазване на стриктен хранителен режим. С времето реших, цялата тази информация и енергия, която съм вложил в себе си, да я предам и на хора, които искат промяна в живота си. Започнах работа в Golden Gym Благоевград през 2019 година, съвсен спонтанно. С времето клиентите се увеличаваха и мен ме радваше факта, че има толкова хора отдадени на идеята за промяна. През 2020 се присъединих към екипът на X Body, където тренировките са доста интензивки и резултатите са двойно бързи. Имам десетки доволни клиенти, с които тренираме и до ден днешен.','Треньор'),(2,'Венцислав Тренчев','trener2blg.jpg','trener2blgModal.jpg','v.trenchev.goldengymbg@mail.bg','Здравейте! Казвам се Венцислав Тренчев на 19 години съм и съм от Благоевград. Към момента съм студент в Американския университет в България със специалности Бизнес и Икономика. Занимавам се с фитнес от пет години като първо започна любителски но после се превърна и в работа. Провеждам индивидуални персонални тренировки по уговорка за дата и час с клиент, като също така мога да правя и групови високо-интензивни тренировки от сорта на Tabata. Активно се занимавам с всякакъв вид спорт, посещавам кикбоксов клуб два пъти в седмицата и играя футбол веднъж. Към тази година провеждам и спортни бягания в Благоевград които са с цел промотирането на здравословния начин на живот сред хората.','Треньор'),(3,'Кирил Стойримски','trener3blg.jpg','trener3blgModal.jpg','k.stoyrimski.goldengymbg@mail.bg','Здравейте! Казвам се Кирил Стоймирски. Персонален треньор съм, основно работя с клиенти целящи сваляне на мазнини, но също се специализирам в силови тренировки. Извън фитнеса имам 4 години опит с канадска борба както и многобройни титли. Когато за първи път отидох на фитнес мразех да спортувам, фитнесът не само че ми помогна да обичам спорта, но ме направи физически и психически по силен. Препоръчвам го на всекиго!','Треньор'),(4,'Симона Войнова','recepciq1blg.jpg','recepciq1blgModal.jpg',NULL,'Здравейте! Казвам се Симона Войнова и съм на 17 години. Живея в Благоевград и уча в Природоматематическа гимназия “Акад. Сергей Корольов”, 11 клас. Въпреки крехката ми възраст, желанието и интересът към спорта допринасят развитието ми. От ранна възраст се занимавам с всякакви разновиностни на спорта. Благодарение на това успях да се изградя като отговорна и дисциплинирана личност. Работя в Golden gum от 3 месеца като рецепционистка на бара, но с времето искам да се развия като инструктор, защото за мен спорта не е само здраве, а и начин на живот.От скоро започнах обучението си за инструктор на Xbody, водена от стремеж и амбиции. Във фитнеса съм заобиколена от професионалистите в екипа, хора, отдадени изцяло на работата, от тях получих прекрасен пример и желание да опитвам и научавам нови неща. Всеотдайна съм, изключително мотивирана, обичам комуникацията с хора и да ги правя щастливи. Това ми доставя удоволствие, а няма по- добър начин да направиш човек щастлив от това да му предоставиш здравословен и активен начин на живот!','Рецепционист');
/*!40000 ALTER TABLE `gym_trainers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-31  0:06:31
