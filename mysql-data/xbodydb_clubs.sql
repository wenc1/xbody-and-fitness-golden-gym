-- MySQL dump 10.16  Distrib 10.1.38-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: xbodydb
-- ------------------------------------------------------
-- Server version	10.5.9-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clubs`
--

DROP TABLE IF EXISTS `clubs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clubs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `main_photo` varchar(265) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(520) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(2560) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clubs`
--

LOCK TABLES `clubs` WRITE;
/*!40000 ALTER TABLE `clubs` DISABLE KEYS */;
INSERT INTO `clubs` VALUES (1,'София','xbody.sofia.goldengym@mail.bg','xbodyClub1.jpg','https://maps.google.com/maps?q=%D0%90%D0%BD%D0%B4%D1%80%D0%B5%D0%B9%20%D0%A1%D0%B0%D1%85%D0%B0%D1%80%D0%BE%D0%B2%20550&t=&z=15&ie=UTF8&iwloc=&output=embed','Xbody Wellness е EMS студио в гр. София. Отваря врати през 2020г., въвеждайки на пазара високо ниво на работа с иновативните Xbody технологии. Професионализмът, знанията и отношението веднага правят впечатление на любителите на спорта, съответно студиото се напълва с доволни клиенти, постигащи с лекота желаната фигура.'),(2,'Благоeвград','xbody.blagoevgrad.goldengym@mail.bg','xbodyClub2.jpg','https://maps.google.com/maps?q=%D0%B1%D0%BB%D0%B0%D0%B3%D0%BE%D0%B5%D0%B2%D0%B3%D1%80%D0%B0%D0%B4%20%D0%BE%D0%B1%D0%B7%D0%BE%D1%80%207&t=&z=15&ie=UTF8&iwloc=&output=embed','Golden Gym MG e лайфстайл фитнес център от най-ново поколение без аналог в страната, оборудван с последна генерация уреди от най-добрите и доказани марки в света. Разполагайки с безупречна база и огромен екип от квалифицирани персонални и групови треньори, които непрестанно се информират и прилагат знанията си за тенденциите в световен мащаб, центърът изгражда популярността си и доказва мястото си в бранша. Заповядайте!'),(3,'Пловдив','xbody.plovdiv.goldengym@mail.bg','xbodyClub2.jpeg','https://maps.google.com/maps?q=%D0%BF%D0%BB%D0%BE%D0%B2%D0%B4%D0%B8%D0%B2%20%D0%B0%D0%BB%D0%B5%D0%BA%D0%BE%20%D0%BA%D0%BE%D0%BD%D1%81%D1%82%D0%B0%D0%BD%D1%82%D0%B8%D0%BD%D0%BE%D0%B2%2015&t=&z=15&ie=UTF8&iwloc=&output=embed','Cryo Xbody Wellness, разположено в центъра на гр. Пловдив, е студио, което отваря врати през 2019г. Селекцията от процедури, които се предлагат са X Body EMS тренировки и антицелулитни процедури, Cryo Sauna и професионални масажи за най-приятния завършек на Вашият ден. Модерният дизайн, висококачествено оборудване и професионалното отношение, с високо ниво на удовлетвореност сред клиентите си, на базата на всичко това е изграден имиджа на най-доброто студио за постигане на мечтаната фигура на територията на града!');
/*!40000 ALTER TABLE `clubs` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-31  0:06:31
