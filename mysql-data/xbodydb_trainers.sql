-- MySQL dump 10.16  Distrib 10.1.38-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: xbodydb
-- ------------------------------------------------------
-- Server version	10.5.9-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `trainers`
--

DROP TABLE IF EXISTS `trainers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trainers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `clubs_id` int(11) DEFAULT NULL,
  `photo` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modal_photo` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(1200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `clubs_id` (`clubs_id`),
  CONSTRAINT `trainers_ibfk_1` FOREIGN KEY (`clubs_id`) REFERENCES `clubs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trainers`
--

LOCK TABLES `trainers` WRITE;
/*!40000 ALTER TABLE `trainers` DISABLE KEYS */;
INSERT INTO `trainers` VALUES (1,'Теодор Бельов',1,'trainer1sofiq.jpeg','trainer1sofiq.jpeg','Здравейте! Казвам се Теодор Бельов, на 24 години. Професионално се занимавам с фитнес от 5 години, като имам тренировъчен стаж над 10 години. По образование съм юрист, като също така съм и сертифициран фитнес инструктор, персонален и ЕМС треньор. Управител съм на CRYO XBODY Wellness Пловдив и XBODY Wellness София. Спортът за мен е начин на живот. Обичам работата си и да помагам на хората да променят живота си към по-добро ме прави щастлив. Най-важното, което трябва да знаете е, че идвайки при нас, вашите цели стават общи и всеки от нас влага цялото си внимание и енергия, за да може тя да бъде постигната. Първата крачка е най-трудна, нека ','Треньор'),(2,'Мари Шуманова',1,'trainer2sofiq.jpeg','trainer2sofiq.jpeg','Усмихната, упорита и отдадена- аз съм Мари. На 21 години , холандски студент, изучаващ широкия обхват на медиите. Тренирам всякакви спортове от ранна детска възраст, а  Хбоди открих преди 2 години, когато имах и удоволствието да се присъединя към екипа на Golden Gym.','Треньор'),(4,'Любомир  Николов',2,'trainer1blagoevgrad.jpg','trainer1blagoevgrad.jpg','Здравейте! Казвам се Любомир Николов на 24 години от Благоевград. Студент съм в Югозападен универистет “Неофит Рилски”, специалност “Право”. Занивам се със спор от ранна възраст. Спортувал съм: лека стлетика, бокс, плуване. С фитнес се занимавам от 16 годишен. Моят баща ме запали по фитнеса, като ми подари книга за бодибилдинг от неговото детство. Нещата бързо прерастнаха от, обикновенно ходене до фитнес залата, до сериозни тренировки и спазване на стриктен хранителен режим. С времето реших, цялата тази информация и енергия, която съм вложил в себе си, да я предам и на хора, които искат промяна в живота си. Започнах работа в Golden Gym Благоевград през 2019 година, съвсен спонтанно. С времето клиентите се увеличаваха и мен ме радваше факта, че има толкова хора отдадени на идеята за промяна. През 2020 се присъединих към екипът на X Body, където тренировките са доста интензивки и резултатите са двойно бързи. Имам десетки доволни клиенти, с които тренираме и до ден днешен.','Треньор'),(5,'Стоян Николов',3,'trainer1plovdiv.JPG','trainer1plovdiv.JPG','Здравейте! Казвам се Стоян Николов на 25 години от Пловдив и съм ХБОДИ инструктор и масажист в CryoXBody Wellness. Вече година и половина успешно прилагам най-модерната технолигия в света на спорта и wellness индустрията, а именно ХБОДИ тренировките. С медицинско образование съм бакалаварска степен РЕХАБИЛИТАЦИЯ и тренировките винаги са ми били страст и хоби едновременно.Всичко започна като една страст към тренировките и се превърна в професионална работа с клиенти като приоритет за вбъдеше ми е да помогна на повече хора да постигнат желаната за тях физика и да се чувстват удовлетворени когато се погледнат о огледалото.','Треньор'),(6,' Василена Кокилев',3,'trainer2plovdiv.JPG','trainer2plovdiv.JPG','Здравейте! Казвам се Василена Кокилева на 22 години от Пловдив и съм оператор на криосауна и рецепционистка в CryoXBody Wellness. Вече две години успешно прилагам най-модерната технолигия в света на спорта и wellness индустрията, а именно криотерапия. Всичко започна с изучаването на лечебната сила на студа и ползите от него. Исках да помогам на повече хора да бъдат здрави, щастливи и да изглеждат добре, което с криотерапията се постига в пъти по-лесно.','Треньор');
/*!40000 ALTER TABLE `trainers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-31  0:06:31
