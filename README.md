# Xbody and Fitness Golden Gym - 04/2021 - current

## 1. Project Overview
A fully responsive website presenting Golden Gym and Xbody training rooms structures.<br></br>
People can book an apointment and order sport products.

## 2. Main Technologies:
JavaScript, React, Express, Mysql, EmailJS, React-Bootstrap

## 3. Prerequisites for Starting the Project:
- Node.js
- VScode
- Mysql
- MariaDB

## 4. Setting Up the Database of the Project
  1. Open Mysql and create schema xbodydb
  2. Open mysql-data folder in project and the tables in to the schema

## 5. Setting Up the BackEnd Server of the Project
  1. Clone this Repository locally and pull it's contents
  3. Open the ```\BackEndXbody``` folder in VScode and in the terminal run ```npm install```
  4. Run  ```npm run start:dev ```
 
## 6. Setting Up The FrondEnd of the project
  1. Open the ```\FrontEnd``` folder in VScode and in the terminal run ```npm install```
  3. In folder ```\FrontEnd-ReactNative\Constants``` open Constansts.js and change url with ```\http://localhost:4000``
  2. To start the app run ```npm start``` - within a few seconds you should see the app open a new tab in your browser

## 7. Photos:
  1. Register Page

 <a href="https://ibb.co/2K2Tr7D"><img src="https://i.ibb.co/KyZJgjM/A422-B9-D8-1-F8-A-4-E80-B2-D0-09-A002-F0-DEB5.png" alt="A422-B9-D8-1-F8-A-4-E80-B2-D0-09-A002-F0-DEB5" border="0"></a>

  2. Login Page

<a href="https://ibb.co/dDpLXt9"><img src="https://i.ibb.co/JHpy8qL/69180-EE2-562-B-49-E2-880-C-A15-D2542341-C.png" alt="69180-EE2-562-B-49-E2-880-C-A15-D2542341-C" border="0"></a>

3. Header menu.

<a href="https://ibb.co/pJwyG9V"><img src="https://i.ibb.co/Ch82NTC/66303-A05-5-B61-432-B-BFDA-0-B3743-EE7-A5-E.png" alt="66303-A05-5-B61-432-B-BFDA-0-B3743-EE7-A5-E" border="0"></a><br />

  4. Homescreen.

<a href="https://ibb.co/ZmC6xrT"><img src="https://i.ibb.co/BNRVBJs/7545-FF9-A-8-A5-E-4-AEF-9-DAE-C3-EA94-C0415-C.png" alt="7545-FF9-A-8-A5-E-4-AEF-9-DAE-C3-EA94-C0415-C" border="0"></a><br />

 5. Golden Gym page

<a href="https://ibb.co/9wM9jGH"><img src="https://i.ibb.co/vstx8D4/gg1.png" alt="gg1" border="0"></a>

<br></br>

<a href="https://ibb.co/YZjPDfp"><img src="https://i.ibb.co/q5170mk/gg2.png" alt="gg2" border="0"></a>

<br></br>

<a href="https://ibb.co/m8fNgZR"><img src="https://i.ibb.co/Hd5h3wK/gg3.png" alt="gg3" border="0"></a>

6. Single Trainer page

<a href="https://ibb.co/vDCdR04"><img src="https://i.ibb.co/pn6K5Yj/st1.png" alt="st1" border="0"></a><br />

<a href="https://ibb.co/MBR7KmQ"><img src="https://i.ibb.co/TWHtJsd/st2.png" alt="st2" border="0"></a><br />

<a href="https://ibb.co/2vH2Dqt"><img src="https://i.ibb.co/7nLcwvW/st3.png" alt="st3" border="0"></a><br />

7. Xbody page

<a href="https://ibb.co/f1hpk1k"><img src="https://i.ibb.co/h7PWB7B/xb1.png" alt="xb1" border="0"></a><br />

<a href="https://ibb.co/dgmYvkB"><img src="https://i.ibb.co/Zm2Drcd/xb2.png" alt="xb2" border="0"></a><br />

8. Single Xbody club page

<a href="https://ibb.co/jMmV5n4"><img src="https://i.ibb.co/LtmQPVp/xbst1.png" alt="xbst1" border="0"></a><br />

<a href="https://ibb.co/hMvHYJD"><img src="https://i.ibb.co/PY84Dvw/xbst2.png" alt="xbst2" border="0"></a><br />
