export const DB_CONFIG = {
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: '1234',
    database: 'xbodydb'
};

export const PORT = 4000;

export const PRIVATE_KEY = 'secret_xbody';
// 60 mins * 60 secs
export const TOKEN_LIFETIME = 1200 * 60;
