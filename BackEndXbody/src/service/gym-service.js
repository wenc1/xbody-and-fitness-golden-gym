const getAllGoldenGymStaff = (gymData) => {
    return async () => {
        const trainers = await gymData.getAllStaff();

        return {
            error: null,
            result: trainers
        }
    }
}

const getSingleStaffGym = (gymData) => {
    return async (id) => {
        const person = await gymData.getStaffBy('id', id);

        return {
            error: null,
            result: person
        }
    }
}

export default {
    getAllGoldenGymStaff,
    getSingleStaffGym
}