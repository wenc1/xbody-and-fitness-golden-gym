const getTrainersByClubId = (trainersData) => {
    return async (id) => {
        const trainers = await trainersData.getBy('clubs_id', id);

        return {
            error: null,
            result: trainers
        }
    }
}

const getSingleTrainer = (trainersData) => {
    return async (id) => {
        const trainers = await trainersData.getBy('id', id);

        return {
            error: null,
            result: trainers
        }
    }
}

export default {
    getTrainersByClubId,
    getSingleTrainer
}