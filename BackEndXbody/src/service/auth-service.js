import bcrypt from 'bcrypt';

const createUser = (authData) => {
    return async (userCreate) => {
        const { username, email, firstName, lastName, phoneNumber, password, rolesId } = userCreate;

        function validateEmail(email) {
            const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        if (username.length < 5 || !validateEmail(email) || !firstName || !lastName || !phoneNumber || !password) {
            return {
                result: null,
                error: "Invalid Value.",
            }
        }

        const existingByEmail = await authData.getBy('email', email);
        const existingByUsername = await authData.getBy('username', username);
        const existingByPhone = await authData.getBy('phone_number', phoneNumber);

        if (existingByEmail || existingByUsername || existingByPhone) {
            return {
                result: null,
                error: "User is registerd yet.",
            }
        }

        const passwordHash = await bcrypt.hash(password, 10);
        const user = await authData.create(username, email, firstName, lastName, phoneNumber, passwordHash, rolesId);

        return { result: await authData.getBy('email', user.email), error: null };
    };
}

const signInUser = (authData) => {
    return async (data, password) => {
        const existingByUsername = await authData.getBy('username', data);
        const existingByEmail = await authData.getBy('email', data);
        const existingByPhone= await authData.getBy('phone_number', data);
        
        if (existingByEmail) {
            const isCorrectPassword = await bcrypt.compare(password, existingByEmail.password);
            if (isCorrectPassword) {
                return {
                    result: await authData.getByWithoutPassword('email', data),
                    error: null
                }
            }
            else {
                return {
                    result: null,
                    error: 'Incorrect email or password'
                }
            }
        }

        else if (existingByUsername) {
            const isCorrectPassword = await bcrypt.compare(password, existingByUsername.password);
            if (isCorrectPassword) {
                return {
                    result: await authData.getByWithoutPassword('username', data),
                    error: null
                }
            }
            else {
                return {
                    result: null,
                    error: 'Incorrect email or password'
                }
            }
        } 

        else if (existingByPhone) {
            console.log("edd1")

            const isCorrectPassword = await bcrypt.compare(password, existingByPhone.password);
            if (isCorrectPassword) {
                return {
                    result: await authData.getByWithoutPassword('phone_number', data),
                    error: null
                }
            } else {
                return {
                    result: null,
                    error: 'Incorrect email or password'
                }
            }
        }
        else {
            console.log("edd")

            return {
                result: null,
                error: 'Incorrect email or password'
            }
        }
    }
}

const sentKey = (authData) => {
    return async (email, key) => {
        const existingByEmail = await authData.getBy('email', email);


        if (!existingByEmail ) {
            return {
                result: null,
                error: "Invalid Email",
            }
        }

        if (!key) {
            return {
                result: null,
                error: "Invalid Key",
            }
        }
        

    
        const keyN = await authData.setKey(email,key);

        return {
            result: "Succsses",
            error: null,
        }
    };
}

const deleteKey  = (authData) => {
    return async (email) => {
        const existingByEmail = await authData.getBy('email', email);


        if (!existingByEmail ) {
            return {
                result: null,
                error: "Invalid Email",
            }
        }

    
        const key = await authData.setKey(email,"");

        return {
            result: "Succsses",
            error: null,
        }
    };
}

const changeUserPassord  = (authData) => {
    return async (email, password, key) => {
        const existingByEmail = await authData.getBy('email', email);

        if (!existingByEmail ) {
            return {
                result: null,
                error: "Invalid Email",
            }
        }

        if(key < 1000 || key > 9999){
            return {
                result: null,
                error: "Wrong key",
            }
        }

        if(existingByEmail['secretkey'] === key ){
            const keyN = await authData.setKey(email,"");
            const passwordHash = await bcrypt.hash(password, 10);
            const change = await authData.changePassword(email,passwordHash)

            return {
                result: "Succsses",
                error: null,
            }
        }
    
        return {
            result: null,
            error: "Wrong key",
        }
        
    };
}

export default {
    createUser,
    signInUser,
    sentKey,
    changeUserPassord,
    deleteKey
}

