import moment from "moment";

const addNewDay = (scheduleData, clubsData) => {
    return async (date, id) => {
        const club = await clubsData.getBy('id', id);

        if (club.length === 0) {
            return {
                error: "Error",
                result: null
            }
        } else {
            const day = await scheduleData.add(new Date(), id);
            return {
                error: null,
                result: "Day is created"
            }
        }
    }
}

const addOrEditHour = (scheduleData) => {
    return async (name, column, id, role, username) => {
        const hours = await scheduleData.getAllForDay(id);
        const hour = await scheduleData.getHour(column, id);
        const hourUser = Object.values(hour[0])[0];

        if (hours.length === 0) {
            return {
                error: "Error",
                result: null
            }
        } else {
            if (role !== "User") {
                const day = await scheduleData.edit(name, column, +id);
            } else {
                if (!hourUser || hourUser.length === 0) {
                    const h = await scheduleData.searchByDate(id)
                    const hArr = Object.values(h)
                    console.log(hArr.indexOf(username))
                    if (hArr.indexOf(username) !== -1) {
                        return {
                            error: "client error",
                            result: null
                        }
                    }
                    const day = await scheduleData.edit(username, column, +id);

                } else {
                    return {
                        error: "Error",
                        result: null
                    }
                }

            }
            return {
                error: null,
                result: "Hour is added"
            }
        }
    }
}


const removeHour = (scheduleData) => {
    return async (column, id, username, role) => {
        const hours = await scheduleData.getAllForDay(id);
        const hour = await scheduleData.getHour(column, id);
        const hourUser = Object.values(hour[0])[0];

        if (hours.length === 0 || !hourUser || hourUser.length === 0) {
            return {
                error: "Error",
                result: null
            }
        } else {
            if (role === "User") {
                if (hourUser === username) {
                    const day = await scheduleData.edit("", column, id);
                }
                else {
                    return {
                        error: "Error",
                        result: null
                    }
                }
            }
            else {
                const day = await scheduleData.edit("", column, id);
            }

            return {
                error: null,
                result: "Hour is removed"
            }
        }
    }
}

const getHourInfo = (scheduleData) => {
    return async (id, column) => {
        const hour = await scheduleData.getHour(column, id);

        if (hour.length === 0) {
            return {
                error: "Error",
                result: null
            }
        }
        return {
            error: null,
            result: hour
        }
    }
}

const getAllHoursForDay = (scheduleData) => {
    return async (id) => {
        const hours = await scheduleData.getAllForDay(id);

        if (hours.length === 0) {
            return {
                error: "Error",
                result: null
            }
        }
        else {
            return {
                error: null,
                result: hours
            }
        }
    }
}

const getLastFourteenDaysForClub = (scheduleData) => {
    return async (id) => {
        const hours = await scheduleData.getLastFourteenDays(id);

        if (hours.length === 0) {
            return {
                error: "Error",
                result: null
            }
        }
        else {
            return {
                error: null,
                result: hours
            }
        }
    }
}

const getAllScheduleForClub = (scheduleData) => {
    return async (id) => {
        const hours = scheduleData.getAllSchedule(id);

        if (hours.length === 0) {
            return {
                error: "Error",
                result: null
            }
        }
        else {
            return {
                error: null,
                result: hours
            }
        }
    }
}


const addNewDateAndGetClubFullInfo = (scheduleData, clubsData, trainersData) => {
    return async (id) => {
        const trainers = await trainersData.getBy('clubs_id', id);
        const club = await clubsData.getBy('id', id);

        if (club.length === 0 || typeof id !== "number") {
            return {
                error: "Error",
                result: null
            }
        }

        let allNewDates = [];

        if (!await scheduleData.getLast(id)) {
            let dateNow = moment().format("DD-MM-YYYY")
            for (let i = 0; i < 14; i++) {
                allNewDates.push(dateNow)
                dateNow = moment(dateNow, "DD-MM-YYYY").add(1, 'days').format("DD-MM-YYYY")
            }

        } else {

            let geToldDate = await scheduleData.getLast(id);
            let oldDate = geToldDate.date;

            const dateNow = moment().add(13, 'days').format("DD-MM-YYYY")

            while (dateNow !== oldDate) {
                oldDate = moment(oldDate, "DD-MM-YYYY").add(1, 'days').format("DD-MM-YYYY")
                allNewDates.push(oldDate)

            }
        }

        if (allNewDates.length < 1) {
            const hours = await scheduleData.getLastFourteenDays(+id);

            return {
                error: null,
                result: {
                    trainers,
                    hours,
                    club
                }
            }
        } else {

            const addDates = await scheduleData.addDate(allNewDates, id);
            const hours = await scheduleData.getLastFourteenDays(id);

            // const club = await clubsData.getBy('id', id)
            return {
                error: null,
                result: {
                    trainers,
                    hours,
                    club
                }
            }

        }
    }
}

export default {
    addNewDay,
    addOrEditHour,
    removeHour,
    getHourInfo,
    getAllHoursForDay,
    getLastFourteenDaysForClub,
    getAllScheduleForClub,
    addNewDateAndGetClubFullInfo
}
