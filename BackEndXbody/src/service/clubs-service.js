const createClub = (clubsData) => {
    return async (name) => {

        const existClub = await clubsData.getBy('name', name);

        if (existClub.length > 0 || typeof name !== "string") {
            return {
                error: "Error",
                result: null
            }
        } else {
            const club = await clubsData.add(name);

            return {
                error: null,
                result: "New club is created"
            }
        }
    }
}

const getAllClubs = (clubsData) => {
    return async () => {

        const clubs = await clubsData.getAll();

        if (clubs.length === 0) {
            return {
                error: "Error",
                result: null
            }
        }

        else {
            return {
                error: null,
                result: clubs
            }
        }
    }
}

const renameClub = (clubsData) => {
    return async (name, id) => {

        const existClub = await clubsData.getBy('id', id);
        const existClubByName = await clubsData.getBy('name', name);

        if (existClubByName.length > 0 || existClub.length === 0
            || typeof name !== "string" || typeof id !== "number") {
            return {
                error: "Error",
                result: null
            }
        } else {
            const club = await clubsData.edit(name, "name", id);

            return {
                error: null,
                result: "The club is renamed"
            }
        }
    }
}

const getSingleClub = (clubsData) => {
    return async (id) => {

        const existClub = await clubsData.getBy('id', id);

        if (existClub.length === 0
            || typeof id !== "number") {
            return {
                error: "Error",
                result: null
            }
        } else {
            return {
                error: null,
                result: existClub
            }
        }
    }
}



export default {
    createClub,
    getAllClubs,
    renameClub,
    getSingleClub
}