import moment from "moment";

const addNewDay = (scheduleGymData, gymData) => {
    return async (date, id) => {
        const club = await gymData.getStaffBy('id', id);

        if (club.length === 0) {
            return {
                error: "Error",
                result: null
            }
        } else {
            const day = await scheduleGymData.add(new Date(), id);
            return {
                error: null,
                result: "Day is created"
            }
        }
    }
}

const addOrEditHour = (scheduleGymData) => {
    return async (name, column, id, role, username) => {
        const hours = await scheduleGymData.getAllForDay(id);
        const hour = await scheduleGymData.getHour(column, id);
        const hourUser = Object.values(hour[0])[0];

        if (hours.length === 0) {
            return {
                error: "Error",
                result: null
            }
        } else {
            if (role !== "User") {
                const day = await scheduleGymData.edit(name, column, id);
            } else {
                if (!hourUser || hourUser.length === 0) {
                    const day = await scheduleGymData.edit(username, column, id);
                } else {
                    return {
                        error: "Error",
                        result: null
                    }
                }

            }
            return {
                error: null,
                result: "Hour is added"
            }
        }
    }
}


const removeHour = (scheduleGymData) => {
    return async (column, id, username, role) => {
        const hours = await scheduleGymData.getAllForDay(id);
        const hour = await scheduleGymData.getHour(column, id);
        const hourUser = Object.values(hour[0])[0];

        if (hours.length === 0 || !hourUser || hourUser.length === 0) {
            return {
                error: "Error",
                result: null
            }
        } else {
            if (role === "User") {
                if (hourUser === username) {
                    const day = await scheduleGymData.edit("", column, id);
                }
                else {
                    return {
                        error: "Error",
                        result: null
                    }
                }
            }
            else {
                const day = await scheduleGymData.edit("", column, id);
            }

            return {
                error: null,
                result: "Hour is removed"
            }
        }
    }
}

const getHourInfo = (scheduleGymData) => {
    return async (id, column) => {
        const hour = await scheduleGymData.getHour(column, id);

        if (hour.length === 0) {
            return {
                error: "Error",
                result: null
            }
        }
        return {
            error: null,
            result: hour
        }
    }
}

const getAllHoursForDay = (scheduleGymData) => {
    return async (id) => {
        const hours = await scheduleGymData.getAllForDay(id);

        if (hours.length === 0) {
            return {
                error: "Error",
                result: null
            }
        }
        else {
            return {
                error: null,
                result: hours
            }
        }
    }
}

const getLastFourteenDaysForClub = (scheduleGymData) => {
    return async (id) => {
        const hours = await scheduleGymData.getLastFourteenDays(id);

        if (hours.length === 0) {
            return {
                error: "Error",
                result: null
            }
        }
        else {
            return {
                error: null,
                result: hours
            }
        }
    }
}

const getAllScheduleForClub = (scheduleGymData) => {
    return async (id) => {
        const hours = scheduleGymData.getAllSchedule(id);

        if (hours.length === 0) {
            return {
                error: "Error",
                result: null
            }
        }
        else {
            return {
                error: null,
                result: hours
            }
        }
    }
}


const addNewDateAndGetClubFullInfo = (scheduleGymData, gymData) => {
    return async (id) => {
        const club = await gymData.getStaffBy('id', id);


        if (club.length === 0 || club[0].position !== "Треньор") {
            return {
                error: "Error",
                result: null
            }
        }
        let allNewDates = [];

        if(club.name === "Любомир Николов"){
            
        }
        if (!await scheduleGymData.getLast(id)) {
            let dateNow = moment().format("DD-MM-YYYY")
           
            for (let i = 0; i < 14; i++) {
                allNewDates.push(dateNow)
                dateNow = moment(dateNow, "DD-MM-YYYY").add(1, 'days').format("DD-MM-YYYY")
            }

        } else {

            let geToldDate = await scheduleGymData.getLast(id);
            let oldDate = geToldDate.date;
            const dateNow = moment().add(13, 'days').format("DD-MM-YYYY")
 
            while (dateNow !== oldDate) {
                oldDate = moment(oldDate, "DD-MM-YYYY").add(1, 'days').format("DD-MM-YYYY")
                console.log(oldDate)
                allNewDates.push(oldDate)

            }
        }

        if (allNewDates.length < 1) {
            const hours = await scheduleGymData.getLastFourteenDays(id);

            return {
                error: null,
                result: {
                    hours
                }
            }
        } else {


            const addDates = await scheduleGymData.addDate(allNewDates, id);
            const hours = await scheduleGymData.getLastFourteenDays(id);

            // const club = await gymData.getBy('id', id)
            return {
                error: null,
                result: {
                    hours,
                }
            }

        }
    }
}

export default {
    addNewDay,
    addOrEditHour,
    removeHour,
    getHourInfo,
    getAllHoursForDay,
    getLastFourteenDaysForClub,
    getAllScheduleForClub,
    addNewDateAndGetClubFullInfo
}
