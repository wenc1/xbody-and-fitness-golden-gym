import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { PORT } from './config.js';
import passport from 'passport';
import jwtStrategy from './auth/strategy.js';
import userController from './controller/user-controller.js';
import adminControler from './controller/admin-controller.js';
import authController from './controller/auth-controller.js';

const app = express();
passport.use(jwtStrategy);

app.use(cors(), bodyParser.json());
app.use(passport.initialize());

app.use('/user', userController);
app.use('/auth', authController);
app.use('/admin', adminControler);

app.use('/public', express.static('images'));


app.all('*', (req, res) =>
  res.status(404).send({ message: 'Resource not found!' })
);

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
