import pool from '../pool.js';

const add = async (name) => {
    const sql = `
        INSERT INTO clubs (name)
        VALUES (?);`

    return await pool.query(sql, [name]);
}

const getBy = async (column, value) => {
    const sql = `
        SELECT * from clubs
        WHERE ${column} = "${value}";`

    return await pool.query(sql);
}

const getAll = async () => {
    const sql = `
        SELECT * from clubs;`

    return await pool.query(sql);
}

const edit = async (name, column, id) => {
    const sql = `
        UPDATE clubs
        SET ${column} = "${name} "
        WHERE id = ${id};`

    return await pool.query(sql);
}


export default {
    add,
    getBy,
    getAll,
    edit
}