import pool from '../pool.js';

const getBy = async (column, value) => {
    const sql = `
        SELECT u.id, u.username, u.email, u.first_name, u.phone_number, u.last_name, u.password, u.secretkey, r.role as role 	FROM users u
        JOIN roles r  ON u.roles_id = r.id
        WHERE u.${column} = '${value}';`;

    const result = await pool.query(sql);

    return result[0];
}

const getByWithoutPassword = async (column, value) => {
    const sql = `
        SELECT u.id, u.username, u.email, u.first_name, u.last_name,u.phone_number, r.role as role 	FROM users u
        JOIN roles r  ON u.roles_id = r.id
        WHERE u.${column} = '${value}';`;

    const result = await pool.query(sql);

    return result[0];
}

const create = async (username, email, firstName, lastName, phoneNumber, password, rolesId) => {
    const sql = `
        INSERT INTO users (username, email, first_name, last_name, phone_number, password, roles_id)
        VALUES (?,?,?,?,?,?,?);
    `;

    const result = await pool.query(sql, [username, email, firstName, lastName, phoneNumber, password, rolesId]);

    return {
        id: result.insertId,
        username: username,
        firstName: firstName,
        lastName: lastName,
        email: email,
    };
};

const getToken = async (token) => {
    const sql = `SELECT *
    FROM token_blacklist 
    WHERE token = ?;`

    return pool.query(sql, [token]);
}

const addToken = async (token) => {
    const sql = ` INSERT INTO token_blacklist(token)
    VALUES (?)`

    return pool.query(sql, [token]);
}

const setKey = async (email, key) => {
    const sql = `
        UPDATE users
        SET secretkey = '${key}' 
        WHERE email = '${email}';`
    return pool.query(sql);
}

const getKey = async (email) => {
    const sql = `
        SELECT secretkey from users
        WHERE email = '${email}';`

    return pool.query(sql);
}

const changePassword = async (email, password) => {
    const sql = `
        UPDATE users
        SET password = '${password}' 
        WHERE email = '${email}';`

    return pool.query(sql);
}

export default {
    getBy,
    getByWithoutPassword,
    create,
    getToken,
    addToken,
    setKey,
    getKey,
    changePassword
}
