import pool from '../pool.js';

const add = async (date, id) => {
    const sql = `
        INSERT INTO schedule (date, clubs_id)
        VALUES (?, ?);`

    return await pool.query(sql, [date, id]);
}

const edit = async (name, column, id) => {
    const sql = `
        UPDATE schedule
        SET ${column} = '${name}'
        WHERE id = ${id};`

    return await pool.query(sql);
}

const getHour = async (column, id) => {
    const sql = `
        SELECT ${column} from schedule
        WHERE id = ${id};`

    return await pool.query(sql);
}

const getAllForDay = async (id) => {
    const sql = `
        SELECT * from schedule
        WHERE id = ${id};`

    return await pool.query(sql);
}

const getLastFourteenDays = async (id) => {
    const sql = `
    SELECT * FROM (
        SELECT * FROM schedule 
        WHERE clubs_id = ${id} 
        ORDER BY id DESC LIMIT 14
    ) sub
    ORDER BY id ASC;`

    return await pool.query(sql);
}

const getAllSchedule = async (id) => {
    const sql = `
    SELECT * FROM schedule WHERE clubs_id = ${id}  ORDER BY id;`

    return await pool.query(sql);
}

const getLast = async (id) => {
    const sql = `
        select * from schedule where clubs_id = ${id} order by id desc limit 1; 
    `;

    const result = await pool.query(sql);

    return result[0];
}

const addDate = async (dates, clubs_id) => {
    const datesToInsert = dates.reduce((acc, d) => {
        acc += `
        ('${d}', '${clubs_id}'),`;
        return acc;
    }, ``);

    const sql = `
        INSERT INTO schedule (date, clubs_id) VALUES ${datesToInsert.slice(0, -1)};`;
    const result = await pool.query(sql);

    return result[0];
}

const searchByDate = async (id) => {
    const sql = `
        select *  from schedule
        where id = ${id};
    `

    const result =  await pool.query(sql);
    return result[0]

}

export default {
    add,
    edit,
    getHour,
    getAllForDay,
    getLastFourteenDays,
    getAllSchedule,
    getLast,
    addDate,
    searchByDate
}