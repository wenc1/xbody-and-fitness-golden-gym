import express from 'express';
import createToken from './../auth/create-token.js';
import authService from '../service/auth-service.js';
import authData from '../data/auth-data.js';

const authController = express.Router();

authController
    .post('/signup', async (req, res) => {
        const createData = req.body;
        const { error, result } = await authService.createUser(authData)(
            createData
        );
        if (error) {
            res.status(409).send({ error: error });
        } else {
            res.status(201).send({ result: result });
        }
    })

    .post('/signin', async (req, res) => {
        const { data, password } = req.body;

        console.log(data)
        console.log(password)
        const { result, error } = await authService.signInUser(authData)(
            data,
            password
        );

        if (error) {
            return res.status(400).send({
                error: error,
            });
        } else {
            const payload = {
                sub: result.id,
                role: result.role,
                username: result.username,
                firstname: result.first_name,
                lastname: result.last_name,
                phone: result.phone_number
            };

            const token = createToken(payload);

            return res.status(200).send({
                token: token,
            });
        }

    })
    .post('/key', async (req, res) => {
        const { email, key } = req.body;

        const { error, result } = await authService.sentKey(authData)(
            email, key
        );
        if (error) {
            res.status(409).send({ error: error });
        } else {
            res.status(201).send({ result: result });
        }
    })
    .delete('/key', async (req, res) => {
        const { email } = req.body;

        const { error, result } = await authService.deleteKey(authData)(
            email
        );
        if (error) {
            res.status(409).send({ error: error });
        } else {
            res.status(201).send({ result: result });
        }
    })
    .put('/password', async (req, res) => {
        const { email, password, key } = req.body;

        const { error, result } = await authService.changeUserPassord(authData)(
            email, password, key
        );
        if (error) {
            res.status(409).send({ error: error });
        } else {
            res.status(201).send({ result: result });
        }
    })


export default authController;