import express from 'express';
import scheduleData from './../data/schedule-data.js';
import scheduleService from './../service/schedule-service.js'
import { authMiddleware, roleMiddleware } from './../auth/auth-middleware.js';
import clubsService from '../service/clubs-service.js';
import clubsData from '../data/clubs-data.js';
import trainersData from './../data/trainers-data.js';
import trainersService from './../service/trainers-service.js'
import gymData from './../data/gym-data.js';
import gymService from './../service/gym-service.js'
import scheduleGymService from '../service/schedule-gym-service.js';
import scheduleGymData from '../data/schedule-gym-data.js';

const userController = express.Router();

userController
    //Get Schedule for 14 days
    .get("/schedule/:id",
        async (req, res) => {
            const { id } = req.params;
            const { error, result } = await scheduleService.getLastFourteenDaysForClub(scheduleData)(+id);

            if (error) {
                res.status(400).send({ error: error });
            }
            else {
                res.status(200).send({ result: result });
            }
        }
    )
    //ADD HOUR
    .post("/schedule/:id/:column",
        authMiddleware,
        async (req, res) => {
            const { id, column } = req.params;
            const { name } = req.body;
            const { role, username } = req.user;

            const { error, result } = await scheduleService.addOrEditHour(scheduleData)(name, column, id, role, username);

            if (error) {
                res.status(400).send({ error: error });
            }
            else {
                res.status(200).send({ result: result });
            }
        }
    )

    //Delete Hour
    .delete("/schedule/:id/:column",
        authMiddleware,
        async (req, res) => {
            const { id, column } = req.params;
            const { role, username } = req.user;
            const { error, result } = await scheduleService.removeHour(scheduleData)(column, id, username, role);

            if (error) {
                res.status(400).send({ error: error });
            }
            else {
                res.status(200).send({ result: result });
            }
        }
    )

    //GET ALL Clubs
    .get("/clubs",
        // authMiddleware,
        async (req, res) => {

            const { error, result } = await clubsService.getAllClubs(clubsData)();

            if (error) {
                res.status(400).send({ error: error });
            }
            else {
                res.status(200).send({ result: result });
            }
        }
    )

    //Get Single Club
    .get("/club/:id",
        // authMiddleware,
        async (req, res) => {
            const { id } = req.params;

            const { error, result } = await clubsService.getSingleClub(clubsData)(+id);

            if (error) {
                res.status(400).send({ error: error });
            }
            else {
                res.status(200).send({ result: result });
            }
        }
    )

    .get(`/trainers/:id`,
        async (req, res) => {
            const { id } = req.params;
            const { error, result } = await trainersService.getTrainersByClubId(trainersData)(+id)
            res.status(200).send({ result: result });

        })
    .get(`/trainer/:id`,
        async (req, res) => {
            const { id } = req.params;
            const { error, result } = await trainersService.getSingleTrainer(trainersData)(+id)
            res.status(200).send({ result: result });

        })
    .get("/club/:clubId/info",
        async (req, res) => {
            const { clubId } = req.params;
            const { error, result } = await scheduleService.addNewDateAndGetClubFullInfo(scheduleData, clubsData, trainersData)(+clubId);

            if (error) {
                res.status(409).send({ error: error });
            } else {
                res.status(201).send({ result: result });
            }

        }
    )
    .get(`/gym/staff`,
        async (req, res) => {
            const { error, result } = await gymService.getAllGoldenGymStaff(gymData)()
            res.status(200).send({ result: result });

        })
    .get(`/gym/staff/:id`,
        async (req, res) => {
            const { id } = req.params;
            const { error, result } = await gymService.getSingleStaffGym(gymData)(id)
            res.status(200).send({ result: result });

        })



    //Get Schedule for 14 days
    .get(`/gym/trainer/schedule/:id`,
        async (req, res) => {
            const { id } = req.params;
            const { error, result } = await scheduleGymService.addNewDateAndGetClubFullInfo(scheduleGymData,gymData,scheduleData)(+id);
            if (error) {
                res.status(400).send({ error: error });
            }
            else {
                res.status(200).send({ result: result });
            }
        }
    )
    //ADD HOUR
    .post("/schedulegym/:id/:column",
        authMiddleware,
        async (req, res) => {
            const { id, column } = req.params;
            const { name } = req.body;
            const { role, username } = req.user;

            const { error, result } = await scheduleGymService.addOrEditHour(scheduleGymData)(name, column, id, role, username);

            if (error) {
                res.status(400).send({ error: error });
            }
            else {
                res.status(200).send({ result: result });
            }
        }
    )

    //Delete Hour
    .delete("/schedulegym/:id/:column",
        authMiddleware,
        async (req, res) => {
            const { id, column } = req.params;
            const { role, username } = req.user;
            const { error, result } = await scheduleGymService.removeHour(scheduleGymData)(column, id, username, role);

            if (error) {
                res.status(400).send({ error: error });
            }
            else {
                res.status(200).send({ result: result });
            }
        }
    )


export default userController;