import express from 'express';
import scheduleData from './../data/schedule-data.js';
import scheduleService from './../service/schedule-service.js'
import clubsService from './../service/clubs-service.js'
import clubsData from './../data/clubs-data.js';
import { authMiddleware, roleMiddleware } from './../auth/auth-middleware.js';

const adminControler = express.Router();

adminControler
    //Create Club
    .post("/club",
        async (req, res) => {
            const { name } = req.body;

            const { error, result } = await clubsService.createClub(clubsData)(name);

            if (error) {
                res.status(400).send({ error: error });
            }
            else {
                res.status(200).send({ result: result });
            }
        }
    )
    .put("/club/:id",
        async (req, res) => {
            const { id } = req.params;
            const { name } = req.body;

            const { error, result } = await clubsService.renameClub(clubsData)(name, +id);

            if (error) {
                res.status(400).send({ error: error });
            }
            else {
                res.status(200).send({ result: result });
            }
        }
    )
    //ADD NEW DAY
    // .post("/schedule/:clubId",
    //     async (req, res) => {
    //         const { clubId } = req.params;
    //         const { error, result } = await scheduleService.addNewDate(scheduleData)(clubId);

    //         if (error) {
    //             res.status(409).send({ error: error });
    //         } else {
    //             res.status(201).send({ result });
    //         }

    //     }
    // )
    //VIEW HOUER INFO
    .get("/schedule/:clubId/:column",
        async (req, res) => {
            const { clubId, column } = req.params;

            const { error, result } = await scheduleService.getHourInfo(scheduleData)(+clubId, column);

            if (error) {
                res.status(400).send({ error: error });
            }
            else {
                res.status(200).send({ result: result });
            }
        }
    )
    //Delete Hour
    .delete("/schedule/:id/:column",
        authMiddleware,
        async (req, res) => {
            const { id, column } = req.params;
            const { role, username } = req.user;
            const { error, result } = await scheduleService.removeHour(scheduleData)(column, id, username, role);

            if (error) {
                res.status(400).send({ error: error });
            }
            else {
                res.status(200).send({ result: result });
            }
        }
    )

export default adminControler;